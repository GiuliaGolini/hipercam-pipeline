# Fitting a 2d polinomial to correct of sky.
#
# This script is written to correct the sky value of images. The problem is that it is
# visible a sort of gradient pattern of sky in the images. Each image has is own gradient.
# Subtracting only a costant value (a plane) to the image would not solve the problem so
# with this python routine we fit a 2d polyfit in x and y axes
# for each image. To do this, this python routine needs as input the masked images corrected of
# bias, flat and gain and then uses astropy.modeling to calculate the polyfit.
#
# Original author:
# Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s): Giulia Golini <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# ---------------------------------------------------------------------------------------------------------------------
# USAGE:
# ---------------------------------------------------------------------------------------------------------------------
# python3 sky-fit.py input_image ext_number output_image pdeg  minvmasking  maxvmasking
#
# PARAMETERS:
# ---------------------------------------------------------------------------------------------------------------------
# input_image   =   str           =   Input name image
# ext_number    =   int           =   Extension number of the input image where the data is
# output_image  =   str           =   Output name model image
# pdeg          =   int           =   Degree of the polynomial fit function
# minvmasking   =   str or float  =   Minimum value of pixels to be considered good (pixels<minvmasking will be masked)
#                                     If minvmasking is 'None' any pixel will be masked.
# maxvmasking   =   str or float  =   Maximum value of pixels to be considered good (pixels>maxvmasking will be masked)
#                                     If maxvmasking is 'None' any pixel will be masked.
# ---------------------------------------------------------------------------------------------------------------------


# Import packages
import warnings
import numpy as np
from astropy.modeling import models, fitting
from astropy.io import fits
import sys





# Define the arguments (input and output image names)
i_input = sys.argv[1]
nhdu = int(sys.argv[2])
i_model = sys.argv[3]
pdeg = int(sys.argv[4])
vmin = sys.argv[5]
vmax = sys.argv[6]



# Open the input image
with fits.open(i_input) as hdul:
    hdul_model = fits.HDUList()
    hdul_model.append(hdul[0])

    z = hdul[nhdu].data
    h1 = hdul[nhdu].header

    y, x = np.mgrid[:z.shape[0], :z.shape[1]]

    # Mask using given value
    if vmin != 'None':
        vmin = float(vmin)
        z[z < vmin] = np.nan

    if vmax != 'None':
        vmax = float(vmax)
        z[z > vmax] = np.nan

    gindex = ~(np.isnan(z))

    # Fit the data using astropy.modeling
    p_init = models.Polynomial2D(degree=pdeg)
    fit_p = fitting.LevMarLSQFitter()

    with warnings.catch_warnings():
        # Ignore model linearity warning from the fitter
        warnings.simplefilter('ignore')
        print(" ")
        print("FITTING ", i_input)
        p = fit_p(p_init, x[gindex], y[gindex], z[gindex])



    # Save the model with the coefficients in the header
    hdu_model = fits.ImageHDU(p(x, y).astype(np.float32))
    hdu_model.header = h1
    hdu_model.header['COMMENT'] = "2D Polynomial fit of degree=" + str(pdeg)
    n = 0
    for c in p.parameters:
        clabel = 'cp_' + str(n)
        hdu_model.header[clabel] = c
        n = n + 1

    hdul_model.append(hdu_model)
    hdul_model.writeto(i_model, overwrite=True)
    print(i_model, "SAVED!")


