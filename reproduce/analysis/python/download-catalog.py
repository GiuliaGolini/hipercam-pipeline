# Download catalog for astrometry and photometry
#
# This Python script downloads datas from a skycatalog in a given region of the sky,
# using Vizier. To more informations I send you to the links :
# https://astroquery.readthedocs.io/en/latest/vizier/vizier.html
# http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=V/147
# This script takes as inputs the coordinates of the center of the region of sky to looking
# for, the width and the height of the region and the catalog selected.
# In this pipeline all these inputs should be set in the inputcatalog.mk file in
# the directory config.
# At the end we have the catalog downloaded and saved with gmag, declination and right ascension
# of the star.
#
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





#------------------------------------------------------------------------------------------------------------------
# USAGE:
#------------------------------------------------------------------------------------------------------------------
# python3 download-catalogue.py RA  DEC  width height catalog_name catalog_output_name
#
# PARAMETERS:
# -----------
# RA		          =   ra                      =   Right ascension coordinates in degrees
# DEC	              =   dec                     =   Declination coordinates in degrees
# width	              =   width                   =   Stars will be searched in the box with this width in minutes
# height              =   height                  =   Stars will be searched in the box with this height in minutes
# catalog_name        =   catalog_name            =   Input name of the catalog with all objects
# catalog_output_name =   catalog_output_name     =   Output name of the catalog with all objects
# ------------------------------------------------------------------------------------------------------------------



# result = Vizier.query_region(coord.SkyCoord(ra=20.86418538, dec=-0.6229532711,unit=(u.deg, u.deg),frame='icrs'),width="3m",height="4m",catalog=["SLOAN"])




# Import packages
import sys
from astroquery.vizier import Vizier
import astropy.units as u
import astropy.coordinates as coord




# Define the arguments (input and output image names)
ra = sys.argv[1]
dec = sys.argv[2]
width = sys.argv[3]
height = sys.argv[4]
catalog_name = sys.argv[5]
catalog_output_name = sys.argv[6]

ra = float(ra)
dec= float(dec)



# Query the region
Vizier.ROW_LIMIT = -1
result = Vizier.query_region(coord.SkyCoord(ra=ra, dec=dec,unit=(u.deg, u.deg) ,frame='icrs'), width=width,height=height ,catalog=[catalog_name])
# print(result)
# print(result['J/ApJ/794/120/catalog'])


# Save the catalog as .fits table
cat = result[catalog_name]



# Save only 3 columns of the catalog, with right ascension, declination and g magnitude
newcat = cat['RA_ICRS','DE_ICRS','gmag']
newcat.write(catalog_output_name, format='fits', overwrite=True)


