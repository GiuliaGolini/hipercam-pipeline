# Age and Metallicity
#
# With this python script we want to identify the age and the metallicity of the galaxy by using the
# models of Magnitudes, colours and mass-to-light ratios in SDSS filters (AB system), PADOVA+00
# CHABRIER.
# http://research.iac.es/proyecto/miles/pages/photometric-predictions-based-on-e-milesseds.php
# The table has to be downloaded before running this script.
# The file is read with genfromtxt.
# The observate magnitudes are read, corrected of dust extinction and plotted with the
# models in the table. Only the best fits will be plotted.
# A fit is chosen by finding those with a U-G,G-R,R-I,I-Z values that are next to the
# observed ones.
# The metallicity and the age corresponding to those best fit will be print in console.
# The best fit should be the one that tells you the age and the metallicity of the source.
#
#
#
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Usage from the shell:
# python3 models.py






# Import modules
import numpy as np
import matplotlib.pyplot as plt





# Read the table
# (To be written in an input file and read with sys)
t=np.genfromtxt('/Users/princess/v-phot-UGC00180/sdss-table.txt', skip_header=1)
#print(t)
# CHECK THE TABLE:
# Before running this script i deleted some rows that are bad; they have bad values for z-band magnitudes.
# Anyway the script should run even if you don't delete them before.





# Observational magnitude values
# (To be written in an input file and read with sys) firsts

#obs_mags = (20.26,19.56,18.94,18.67,18.4)



# They have to be corrected of dust extincion!!
# the values are found here  https://ned.ipac.caltech.edu/conesearch?search_type=Near%20Position%20Search&coordinates=20.86418538h%20-0.6229532711d&radius=1&in_csys=Equatorial&in_equinox=J2000&out_csys=Equatorial&out_equinox=Same%20as%20Input&hconst=67.8&omegam=0.308&omegav=0.692&wmap=4&corr_z=1&iau_style=liberal&in_csys_IAU=Equatorial&in_equinox_IAU=B1950&z_constraint=Unconstrained&z_unit=z&ot_include=ANY&nmp_op=ANY&obj_sort=Distance%20to%20search%20center
# NED vebsite, set the coordinates of the source, first result, galactic extinction of SSDS
# in this region we have different values of each filter.
# Observational magnitude values corrected of extincion.
#ext_coeff = (0.429,0.335,0.232,0.172,0.128)

umag = 20.15  -0.429
gmag = 19.75  -0.335
rmag = 19.12  -0.232
imag = 18.84  -0.172
zmag = 18.48  -0.128
#corr-mags= obs_mags-ext_coeff


# Set magnitudes differences
ug = umag-gmag
gr = gmag-rmag
ri = rmag-imag
iz = imag-zmag

print ( "U-G",ug, "G-R",gr, "R-I", ri,"I-Z",iz)


# tollerance for the fit to datas ( should be 0.1 )

# arbitrary assumptions
toll_u = 0.5
toll_g = 0.5
toll_r = 0.5
toll_i = 0.5
toll_z = 0.5

toll_ug = np.sqrt(toll_u*toll_u + toll_g*toll_g)
toll_gr = np.sqrt(toll_r*toll_r + toll_g*toll_g)
toll_ri = np.sqrt(toll_r*toll_r + toll_i*toll_i)
toll_iz = np.sqrt(toll_i*toll_i + toll_z*toll_z)
#print (toll_ri)

d = 100000000 # 100 Mp
# Modulo di distanza
Mod_dist = -5 + 5*np.log10(d)
print("m-M",Mod_dist)
# Magnitudine assoluta in R
Mr = rmag - Mod_dist
print("Mr",Mr)

# Per la massa ho bisogno della relazione massa luminosità in r che ho in tabella "mlr",t[i,12]
# https://arxiv.org/pdf/0807.2776.pdf BAKOS, TRUJILLO, & POHLEN
# COLOR PROFILES OF SPIRAL GALAXIES: CLUES ON OUTER-DISK FORMATION SCENARIOS
log_Mass = np.log(0.13)-0.4*(Mr-4.65)+8.629
print("Mass",log_Mass)

# Array y axes, the values of the filters magnitude
# Old values with negative numbers mags = (20.26 - 0.429,19.56-0.335,18.94-0.232,18.67-0.172,18.40-0.128)
# Masked 1 sigma and negative numbers
mags = ( 20.15-0.429 ,19.75-0.335,19.12-0.232,18.84-0.172,18.48-0.128)

#  Effective central wavelength of each filter U G R I Z (x axes)
lamda = (3580,4754,6204,7698,9665)


# nrow= 284 number of row in the table
'''
for i in range(0,284):
    # Only the best fits will be plotted.
    # A fit is chosen by finding those with U-G,G-R,R-I,I-Z values that are next to the
    # observed ones.
    if  t[i,5]-t[i,6] > ug-toll_ug and t[i,5]-t[i,6] < ug+toll_ug and t[i,6]-t[i,7] > gr-toll_gr and t[i,6]-t[i,7] < gr+toll_gr and t[i,7]-t[i,8] > ri-toll_ri and t[i,7]-t[i,8] < toll_ri+0.2  and t[i,8]-t[i,9] > iz-toll_iz and t[i,8]-t[i,9] < iz+toll_iz :
        y = t[i,5:10]
        # To have a readable graph, table's values have to be normalized to the observed ones. I choosed the r-band for the normalization. As a normalization factor I will take the difference betveen the observed rmag and the rmag of each model. Then add this factor to the fit.
        normfactor = rmag - t[i,7]
        y = y + normfactor
        print("model",i,"age",t[i,3],"z=",t[i,2], "mlr",t[i,12])
        # This is the SED, spectral energy distribution. (y axes reverse order)
        plt.title('SED')
        plt.xlabel('Lambda A')
        plt.ylabel('U G R I Z')
        plt.ylim(21,18)
        plt.plot(lamda,y)

'''
chi = []
for i in range (0,284):
    y = t[i,5:10]
    normfactor = rmag-t[i,7]
    y = y + normfactor
    val = np.sum([(umag-(t[i,5]+ normfactor))**2,(gmag-(t[i,6]+ normfactor))**2 ,(rmag-(t[i,7]+ normfactor))**2,(imag-(t[i,8]+normfactor))**2 ,(zmag-(t[i,9]+normfactor))**2 ])
    chi.append(val)
chi_min= np.amin(chi)
print(chi_min)

for i in range (0,284):
   if chi[i] == chi_min:
       print( "best fit" ,i,"age",t[i,3],"z=",t[i,2], "mlr",t[i,12])
       y = t[i,5:10]
       normfactor = rmag-t[i,7]
       y = y + normfactor
       plt.xlabel('Lambda A')
       plt.ylabel('U G R I Z')
       plt.ylim(21,18)
       plt.plot(lamda,y)



'''imodel_min = np.where(chi == chi_min) #number
print(imodel_min[0][0])


y =t[imodel_min[0][0],5:10]
print(y)
plt.figure(2)
plt.ylim(21,18)
plt.plot(lamda,y)'''

# Plot of observed values
plt.ylim(21,18)
plt.scatter(lamda,mags)

plt.show()
