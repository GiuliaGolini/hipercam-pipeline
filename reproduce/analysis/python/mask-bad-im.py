# Mask with nans the bad blocks' images
#
# This Python script is useful when you have any defects in the input images.
# In my case a corner of a window in a block of observations, in all filters and in all images,
# is bad. This can bring errors during the reduction, for example a bad detection of the flat,
# a bad construption of the sky model of the images...
# With this script the defect is masked with nans as to not have any
# problem during the following steps of the reduction.
# With this script you can mask an arbitrary numer of columns on the left part of the image.
# This script should be modified after deciding which part of the image is bad.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





#---------------------------------------------------------------------------
# USAGE:
#---------------------------------------------------------------------------
# python3 mask-bad-im.py input_image output_image ncol nrow
#
# PARAMETERS:
# -----------
# input_image         =   im                   =   Input name image
# output_image        =   im_out               =   Output name good image
# nrow                =   nrow                 =   Number of row to mask
# ncol                =   ncol                 =   Number of columns to mask
#---------------------------------------------------------------------------




# Import packages
import numpy as np
from astropy.io import fits
import os
import sys





# Define the arguments
im = sys.argv[1]
im_out = sys.argv[2]
ncol = int(sys.argv[3])
#nrow = int(sys.argv[4])


# Read the input fits image naming it as `hdulist'
hdulist = fits.open(im)





# Define the Number of Filters.
# Each number correspond to a
# different filter.
nfilterwindows = (1,2,3,4,5)


# Define a new empty HDU list
hdulist_out = fits.HDUList()
hdulist_out.append(hdulist[0])


# This bugle allows to read each extension of the image.
# Each extension corresponds to a filter.
for i in nfilterwindows:
    
    # Read the windows for each filter
    good_image = hdulist[i].data   #  window data
    print(good_image)
    
    # Mask with nans the chosed part of the image
    good_image[:,:ncol] = np.nan
    
    # Save the image
    hduout = fits.ImageHDU(good_image)
    
    # Fill the hdulist_out append the generated hduout
    hdulist_out.append(hduout)

# Save the corrected image
hdulist_out.writeto(im_out, overwrite=True)


