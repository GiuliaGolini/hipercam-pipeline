# Deleting the U images from the filters folder.
# We do this because in this case we still do not have fixed the problem with the 
# U filter astrometry.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# ------
# USAGE:
# ------
# python3 delete-u.py input_image output_image 
#
# PARAMETERS:
# -----------
# input_image   =   im            =   Input name image
# output_image  =   im_out        =   Output name model image
# --------------------------------------------------------------------------------------------------------------





# Import packages
import numpy as np
from astropy.io import fits
# import matplotlib.pyplot as plt
import os
import sys
import shutil
from shutil import copyfile




# Define the arguments (input and output image names)
im = sys.argv[1]
im_out = sys.argv[2]

copyfile(im, im_out)
os.remove(im)

