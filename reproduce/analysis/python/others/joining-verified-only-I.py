# Structure for joining windows in Hipercam images and correct the gain error.
# with median values coefficients.
#
# This script is written to have only a image for each CCD camera of HiPERCAM.
# This is needed because every CCD is composed of 4 windows (E1, F1, G1, H1).
# We read the windows left bottom, right bottom, right top, to left top window.
# CCD 1, CCD 3, CCD 4 have the same structure : E1, F1, G1, H1
# CCD 2, CCD 5 have the same structure : E1, F1, H1, G1
# To correct this problem at the beginning of the loop we define a nfilterwindows array
# changing the position of the G1 H1 windows in the 1, 3 and 4 CCD and changing the position
# of E1, F1 windows in 2 and 5 CCDs.
#
# Moreover the script allows to correct the imperfect matching of windows images caused by
# the Gain error.
# Take a number (ncolrow) of columns at the end of the left side window and the same
# number of columns at the beginning of the right side one. Then compute the median
# of the pixel values of the columns in each image and 
# calculate the median of the two medians. Now assume that the right image is correct
# and multiply the left one for the final median value.
# The same is done for the upper images.
# Then take the last tree row of the combined upper image and the upper three row
# of the bottom combined image and compute each median and the median of the quotient
# of the two medians. Assume that the bottom one is ok and multiply the upper one
# to the final median.
#
# We modified this script because the gain correction is not well done in the U filter.
# The problem is that the histogram of the sigma-clip-values of the coefficients
# for upper and bottom/upper correction shows two higher levels, is not a only picked
# curve.
# So the verified correction is done in all filters except in U filter, and only when
# combining the bottom images the sigmaclip mean value is taken.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Python script is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Python script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# ------
# USAGE:
# ------
# python3 joining-verified.py input_image output_image sig_factors ncolrow
#
# PARAMETERS:
# -----------
# input_image         =   im                   =   Input name image
# output_image        =   im_out               =   Output name model image
# sig_factors         =   sig_factors          =   Sigma clip mean values of the corrections
# ncolrow             =   ncolrow              =   Number of raw/columns to median for the gain correction
#--------------------------------------------------------------------------------------------------------------





# Import packages
import numpy as np
from astropy.io import fits
import os
import sys





# Define the arguments (input and output image names)
im = sys.argv[1]
im_out = sys.argv[2]
sig_factors = sys.argv[3]
ncolrow = int(sys.argv[4])


# Need to read the txt file as an array
factors = np.genfromtxt(sig_factors)
factors = np.array(factors)
print(factors)



# Read the input fits image naming it as `hdulist'
hdulist = fits.open(im)





# Define the Number of Filter Windows
# Each set of numbers correspond to a
# different filter
nfw = ( [ 1,  2,  4,  3, "i"])


# Define a new empty HDU list
hdulist_out = fits.HDUList()
hdulist_out.append(hdulist[0])





# Each number of filter window (nfw) is a tuple with the
# number of extensions that belong to a given filter
# Read the four windows for a given filter
d_bl = hdulist[nfw[0]].data   # Bottom left window data
d_br = hdulist[nfw[1]].data   # Bottom right window data
d_ul = hdulist[nfw[2]].data   # Upper left window data
d_ur = hdulist[nfw[3]].data   # Upper right window data

# Do the joining of the four windows
# Bottom
# First loop i = 0 corresponding to coeff 1 in u filter
# Second loop i = 1 corresponding to coeff 1 in g filter and so on..
fb = factors[0]
db = np.concatenate((d_bl, d_br*fb), axis=1)
# Upper
# First loop i = 5 corresponding to coeff2 in u filter
# Second loop i = 6 corresponding to coeff2 in g filter and so on..
fu = factors[1]
du = np.concatenate((d_ul, d_ur*fu), axis=1)
# Bottom and Upper
# First loop i = 10 corresponding to coeff3 in u filter
# Second loop i = 11 corresponding to coeff3 in g filter and so on..
fbu = factors[2]
dbu = np.concatenate((db*fbu, du), axis=0)
# Save the ccd image
hduout = fits.ImageHDU(dbu)
hduout.name = "ext_" + nfw[-1]

# Fill the hdulist_out append the generated hduout

hdulist_out.append(hduout)

# Save the corrected image
hdulist_out.writeto(im_out, overwrite=True)


