# Make the astrometry for U filter
#
# The problem in the U filter's images is that we can detect only few stars in 
# each image (less than 10 sometime). With those few stars solve-field cannot build 
# a correct catalog to make astrometry.
# First solution was to lower the odds to solve level (which is 1e6 default). Not working.
# By lowing the code tolerance the astrometry is not correct. Some images are not placed
# in the right position of the sky.
# A second solution could be to use the center of each astrometrized image in G filter and
# give the values of RA and DEC of the center ( with the radius around the center
# where to looking for) to solve field to run the astrometry.
# This ca be done by defining the rule to create the text file of each image with the right 
# ascension coordinate and declination.
# This is done by using astfits $< -h1 |grep CRVAL1 (CRVAL2) > $@.
# then giving to solve field the parameters --ra --dec with aststatistics $(word 3, $^) -c3 --mean)
# and the --radius where look for.
# This is not working so good anyway. The stars are too few.
# The option we use is to build a moffattian function (kernel.fits) and apply it to each image in
# U band. Meaning do a spatial convolution . The final image is smoothed, and we can see more sources.
# Finally we apply solve field to the convolved images to make the astrometry. The astrometry is finally 
# sent to the origina ( not convolved) images.

# This Makefile is written to do the astrometry of U filters image using 
# solve-field routine.
# The main goal is to have a template to select the images and add the astrometry.
# This website recall how solve field works:
# https://www.mankier.com/1/solve-field
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Directories
# -----------
#
# U-FILTER
# -----------
#
# Define the directory with the u filter images
u-indir = $(BDIR)/u-filter
# Define the directory where the buid u images astrometrized
u-outdir = $(BDIR)/u-astrometry




# Opition 1 : directly with astfits

#ra-coord=$$(astfits $(word 2, $^) --hdu=1 | grep CRVAL1)
#dec-coord=$$(astfits  $(word 2, $^) --hdu=1 |grep CRVAL2)





# Option 2 : put all the coordinates in a file an read from it
solve-u-ra-outdir = $(BDIR)/ra-g-coordinates
solve-u-dec-outdir = $(BDIR)/dec-g-coordinates
#Directory where we take the astrometrized images of g filter
g-indir = $(BDIR)/g-astrometry


# Define the rule to create the text file of each image with the right ascension coordinate
$(solve-u-ra-outdir):; mkdir $@
ra-coords = $(foreach inpu,$(base-names),$(solve-u-ra-outdir)/$(inpu).txt)
$(ra-coords): $(solve-u-ra-outdir)/%.txt : $(g-indir)/%.fits  |$(solve-u-ra-outdir)
	astfits $< -h1 |grep CRVAL1 > $@

# Define the rule to create the text file of each image with the declination coordinate
$(solve-u-dec-outdir):; mkdir $@
dec-coords = $(foreach inpu,$(base-names),$(solve-u-dec-outdir)/$(inpu).txt)
$(dec-coords): $(solve-u-dec-outdir)/%.txt : $(g-indir)/%.fits  |$(solve-u-dec-outdir)
	astfits $< -h1 |grep CRVAL2 > $@









# Define the rule to make the kernel file to do smoothing: 
# we use a molfattian function for the convolution
k-outdir = $(BDIR)/kernel
$(k-outdir):; mkdir $@
$(k-outdir)/kernel.fits : |$(k-outdir)
	astmkprof --kernel=moffat,3,2.8,5 --oversample=1 --output=$@ 


# Define the rule to create the smoothed images 
# The convolution can be done separately o directly in the rule before solvefield.
#
#conv-u-outdir = $(BDIR)/convolved-im-u
#$(conv-u-outdir):; mkdir $@
#conv-u-images = $(foreach inpu,$(base-names),$(conv-u-outdir)/$(inpu).fits)
#$(conv-u-images): $(conv-u-outdir)/%.fits : $(u-indir)/%.fits $(k-outdir)/kernel.fits | $(conv-u-outdir)
#	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@ 




# Define the rule to make astrometry
ca-astrometry-cfg = $(BDIR)/download-catalog/astrometry.cfg


$(u-outdir):; mkdir $@
u-astr-ims = $(foreach uname, $(base-names), $(u-outdir)/$(uname).fits)
$(u-astr-ims): $(u-outdir)/%.fits: $(u-indir)/%.fits $(k-outdir)/kernel.fits | $(u-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                  \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 1e4                        \
                --dir $(u-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)         

	#--sextractor-config /Users/princess/final/Hipercam_pipeline/reproduce/analysis/astromatic/default.sex \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@    # da cambiare!!!
	
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)











# Reference catalog needed



# Obtain the astrometry with `solve-field' for U filter
# -----------------------------------------------------
#
# The prerequisites are the coordinates of the central point of 
#each G filter image and the un-astrometrized images in U filter.


#$(u-outdir):; mkdir $@
#u-astr-ims = $(foreach uname, $(base-names), $(u-outdir)/$(uname).fits)
#$(u-astr-ims): $(u-outdir)/%.fits: $(u-indir)/%.fits $(g-indir)/%.fits | $(u-outdir)
#	r=$$(astfits $(word 2, $^) --hdu=1 | grep CRVAL1)
#	d=-0.6
#	solve-field $<                                         \
#				--no-plots                                 \
 #               --overwrite                                \
  #              --extension 1                              \
	#	        --scale-low 0.05                           \
	#	        --scale-high 0.09                          \
	#	        --odds-to-solve  0.7                       \
	#	        --use-sextractor                           \
	#	        --sextractor-config /Users/princess/final/Hipercam_outputs/software/installed/share/sextractor/default.sex \
	#	        --x-column X_IMAGE --y-column Y_IMAGE \
	#	        --sort-column MAG_AUTO   \
	#	        --sort-ascending  \
	#	        --ra $$r \
	#	        --dec $$d \
	#	        --radius 1 \
	#	        --verbose \
     #           --dir $(u-outdir)                          \
	#	        --scale-units arcsecperpix                 \
	#	        --config $(ca-astrometry-cfg)              \
	#	        --wcs $(subst .fits,_wcs.fits,$@)          





#solve-field $<                                      \
#				--no-plots                                 \
 #               --overwrite                                \
  #              --extension 1                              \
	#	        --scale-low 0.07                           \
	#	        --scale-high 0.09                          \
	#	        --odds-to-solve  0.7                         \
	#	        --use-sextractor                           \
	#	        --ra $$(aststatistics $(word 2, $^) -c3 --mean)  \
	#	        --dec $$(aststatistics $(word 3, $^) -c3 --mean) \
	#	        --radius 0.02 \
	#	        --verbose \
     #           --dir $(u-outdir)                          \
	#	        --scale-units arcsecperpix                 \
	#	        --config $(ca-astrometry-cfg)              \
	#	        --wcs $(subst .fits,_wcs.fits,$@)   





#	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
#	rm $(subst .fits,.axy,$@)
#	rm $(subst .fits,.corr,$@)
#	rm $(subst .fits,-indx.xyls,$@)
#	rm $(subst .fits,.match,$@)
#	rm $(subst .fits,.new,$@)
#	rm $(subst .fits,.rdls,$@)
#	rm $(subst .fits,.solved,$@)
#	rm $(subst .fits,_wcs.fits,$@)






# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#
$(mtexdir)/solve-field-u.tex: $(u-astr-ims)  
	touch $@


