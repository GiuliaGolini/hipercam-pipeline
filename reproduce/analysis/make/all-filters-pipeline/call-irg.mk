# Call IRG python routine to build colors image
#
# This Makefile is written to build the RGB coloured image of the source.
# The main goal is to have a template to call the python routine which stacks the
# different filters images.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid image is going to be
rgb-outdir = $(BDIR)/RGB






# Call the python routine to stack images
# ---------------------------------------
#
# The stacked image will be putted in another directory with the name rgb.fits.
# To build this we need an input file with the names of the images to stack and the output name
# of the final image.
$(rgb-outdir): ; mkdir $@
$(rgb-outdir)/rgb.fits: reproduce/analysis/config/stacked-images.mk | $(rgb-outdir)
	# Call the python routine 
	python3 reproduce/analysis/python/IRG.py $(catstripe1) $@ 






# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/call-irg.tex: $(rgb-outdir)/rgb.fits
	touch $@


