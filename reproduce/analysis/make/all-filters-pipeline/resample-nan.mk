# Resampling of the images with Nans
#
# This Makefile is written to substitute the Nan value for the pixels with
# value =0. This is because when doing the final coadd the 0 values wuold
# affect the final value of the astarithmetic sigma-clip mean. Substituting 
# the Nan values allows to not take into account those pixeles.
# This is done with astarithmetic.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# R directories
res-r-nan-indir = $(BDIR)/r-resampled
res-r-nan-outdir = $(BDIR)/r-nan-resampled





# Nan resampled images R-filter
# -----------------------------
#
# Pattern match for the "Nan" resampled images. 
res-r-nan-images = $(foreach res-r,$(base-names),$(res-r-nan-outdir)/$(res-r).fits)
# Substituting Nans on R filter where there are zeros and build the output directory.
# The rule to create the resampled images uses astarithmetic on the resampled images.
# --hdu=0 because we have only one extension of the resampled image with swarp.
# and this is =0, astarithmetic read the value 1 as default.
$(res-r-nan-outdir):; mkdir $@
$(res-r-nan-images): $(res-r-nan-outdir)/%.fits: $(res-r-nan-indir)/%.fits | $(res-r-nan-outdir)
	astarithmetic $< --hdu=0 $< --hdu=0 0 eq nan where --output=$@





# I directories
res-i-nan-indir = $(BDIR)/i-resampled
res-i-nan-outdir = $(BDIR)/i-nan-resampled



# Nan resampled images I-filter
# -----------------------------
#
# Pattern match for the "Nan" resampled images. 
res-i-nan-images = $(foreach res-i,$(base-names),$(res-i-nan-outdir)/$(res-i).fits)
# Substituting Nans on R filter where there are zeros and build the output directory.
# The rule to create the resampled images uses astarithmetic on the resampled images.
# --hdu=0 because we have only one extension of the resampled image with swarp.
# and this is =0, astarithmetic read the value 1 as default.
$(res-i-nan-outdir):; mkdir $@
$(res-i-nan-images): $(res-i-nan-outdir)/%.fits: $(res-i-nan-indir)/%.fits | $(res-i-nan-outdir)
	astarithmetic $< --hdu=0 $< --hdu=0 0 eq nan where --output=$@





# G directories
res-g-nan-indir = $(BDIR)/g-resampled
res-g-nan-outdir = $(BDIR)/g-nan-resampled




# Nan resampled images G-filter
# -----------------------------
#
# Pattern match for the "Nan" resampled images. 
res-g-nan-images = $(foreach res-g,$(base-names),$(res-g-nan-outdir)/$(res-g).fits)
# Substituting Nans on R filter where there are zeros and build the output directory.
# The rule to create the resampled images uses astarithmetic on the resampled images.
# --hdu=0 because we have only one extension of the resampled image with swarp.
# and this is =0, astarithmetic read the value 1 as default.
$(res-g-nan-outdir):; mkdir $@
$(res-g-nan-images): $(res-g-nan-outdir)/%.fits: $(res-g-nan-indir)/%.fits | $(res-g-nan-outdir)
	astarithmetic $< --hdu=0 $< --hdu=0 0 eq nan where --output=$@





# Z directories
res-z-nan-indir = $(BDIR)/z-resampled
res-z-nan-outdir = $(BDIR)/z-nan-resampled




# Nan resampled images Z-filter
# -----------------------------
#
# Pattern match for the "Nan" resampled images. 
res-z-nan-images = $(foreach res-z,$(base-names),$(res-z-nan-outdir)/$(res-z).fits)
# Substituting Nans on R filter where there are zeros and build the output directory.
# The rule to create the resampled images uses astarithmetic on the resampled images.
# --hdu=0 because we have only one extension of the resampled image with swarp.
# and this is =0, astarithmetic read the value 1 as default.
$(res-z-nan-outdir):; mkdir $@
$(res-z-nan-images): $(res-z-nan-outdir)/%.fits: $(res-z-nan-indir)/%.fits | $(res-z-nan-outdir)
	astarithmetic $< --hdu=0 $< --hdu=0 0 eq nan where --output=$@





# U directories
res-u-nan-indir = $(BDIR)/u-resampled
res-u-nan-outdir = $(BDIR)/u-nan-resampled

# Nan resampled images U-filter
# -----------------------------
#
# Pattern match for the "Nan" resampled images. 
res-u-nan-images = $(foreach res-u,$(base-names),$(res-u-nan-outdir)/$(res-u).fits)
# Substituting Nans on R filter where there are zeros and build the output directory.
# The rule to create the resampled images uses astarithmetic on the resampled images.
# --hdu=0 because we have only one extension of the resampled image with swarp.
# and this is =0, astarithmetic read the value 1 as default.
$(res-u-nan-outdir):; mkdir $@
$(res-u-nan-images): $(res-u-nan-outdir)/%.fits: $(res-u-nan-indir)/%.fits | $(res-u-nan-outdir)
	astarithmetic $< --hdu=0 $< --hdu=0 0 eq nan where --output=$@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#$(sw-outdir)/coadd-r-filter.fits $(sw-outdir)/coadd-i-filter.fits $(sw-outdir)/coadd-z-filter.fits

$(mtexdir)/resample-nan.tex:  $(res-g-nan-images)  
	touch $@




