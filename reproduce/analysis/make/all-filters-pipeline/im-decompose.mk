# Decompose the images
#
# This Makefile is written to do the decomposition of images taken from HiPERCAM instrument.
# The main goal is to have a template to separate the images of each of the 5 filters of HiPERCAM.
# This is useful to allow us to use the "solve-field" routine to do the astrometry for each filter. 
# To do the decomposition a loop is needed.
# Once compiled this routine
# every image in U filter corresponds to the extension 1,
# every image in G filter corresponds to the extension 2,
# every image in R filter corresponds to the extension 3,
# every image in I filter corresponds to the extension 4,
# every image in Z filter corresponds to the extension 5.

#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Directories
# -----------
#
# Define the directory where we have the corrected images 
ca-indir  = $(BDIR)/corrected-sky-plane






# Define each filter directory where the build images are going to be
dec-u = $(BDIR)/u-filter
dec-g = $(BDIR)/g-filter
dec-r = $(BDIR)/r-filter
dec-i = $(BDIR)/i-filter
dec-z = $(BDIR)/z-filter





# Decompose only U filter
#------------------------
#
$(dec-u):; mkdir $@
u-filter-images = $(foreach name, $(base-names), $(dec-u)/$(name).txt)
$(u-filter-images): $(dec-u)/%.txt: $(ca-indir)/%.fits | $(dec-u)
	tmpim=$(subst .txt,.fits,$@)               \
	&& astfits $< --copy=1 --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@    \
	# Inject the prerequisite into the target
	echo $< > $@





# Decompose only G filter
#------------------------
#
$(dec-g):; mkdir $@
g-filter-images = $(foreach name, $(base-names), $(dec-g)/$(name).txt)
$(g-filter-images): $(dec-g)/%.txt: $(ca-indir)/%.fits | $(dec-g)
	tmpim=$(subst .txt,.fits,$@)               \
	&& astfits $< --copy=2 --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@    \
	# Inject the prerequisite into the target
	echo $< > $@





# Decompose only R filter
#------------------------
#
$(dec-r):; mkdir $@
r-filter-images = $(foreach name, $(base-names), $(dec-r)/$(name).txt)
$(r-filter-images): $(dec-r)/%.txt: $(ca-indir)/%.fits | $(dec-r)
	tmpim=$(subst .txt,.fits,$@)               \
	&& astfits $< --copy=3 --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@    \
	# Inject the prerequisite into the target
	echo $< > $@





# Decompose only I filter
#------------------------
#
$(dec-i):; mkdir $@
i-filter-images = $(foreach name, $(base-names), $(dec-i)/$(name).txt)
$(i-filter-images): $(dec-i)/%.txt: $(ca-indir)/%.fits | $(dec-i)
	tmpim=$(subst .txt,.fits,$@)               \
	&& astfits $< --copy=4 --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@    \
	# Inject the prerequisite into the target
	echo $< > $@





# Decompose only Z filter
#------------------------
#
$(dec-z):; mkdir $@
z-filter-images = $(foreach name, $(base-names), $(dec-z)/$(name).fits)
$(z-filter-images): $(dec-z)/%.fits: $(ca-indir)/%.fits | $(dec-z)
	astfits $< --copy=5 --output=$@    \






# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all the images of filters
# have been obtained.
#
$(mtexdir)/im-decompose.tex:  $(g-filter-images) $(r-filter-images) $(i-filter-images) 
	touch $@




