# Resampling of the images
#
# This Makefile is written to resample the images of a single filter with swarp.
# The main goal is to have a template to use swarp on astrometrized images and
# resampling it on a bigger image, an image with the dimension of the final 
# coadded one. This allow us to stack the images after, with a sigma-clip mean,
# that we cannot do automatically with swarp.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Directories
# -----------
#
# R directories
res-r-indir = $(BDIR)/r-astrometry
res-r-outdir = $(BDIR)/r-resampled


# Resampled images R-filter
# -------------------------
#
# Pattern match for the resampled images. 
res-r-images = $(foreach resr,$(base-names),$(res-r-outdir)/$(resr).fits)

# Resample R filter and build the output directory.
# The default.swarp file should be modified : you can choose manually (CENTER TYPE = MANUAL), 
# the coordinates to resample and the dimension of the output images . 
# In this case should be 4084 4484.
# The rule to create the resampled images uses swarp with the astrometrized images.
$(res-r-outdir):; mkdir $@
$(res-r-images): $(res-r-outdir)/%.fits: $(res-r-indir)/%.fits | $(res-r-outdir)
	swarp $< -c reproduce/analysis/astromatic/default.swarp -IMAGEOUT_NAME $@ 





# I directories
res-i-indir = $(BDIR)/i-astrometry
res-i-outdir = $(BDIR)/i-resampled


# Resampled images I-filter
# -------------------------
#
# Pattern match for the resampled images. 
res-i-images = $(foreach resr,$(base-names),$(res-i-outdir)/$(resr).fits)

# Resample R filter and build the output directory.
# The default.swarp file should be modified : you can choose manually (CENTER TYPE = MANUAL), 
# the coordinates to resample and the dimension of the output images . 
# In this case should be 4084 4484.
# The rule to create the resampled images uses swarp with the astrometrized images.
$(res-i-outdir):; mkdir $@
$(res-i-images): $(res-i-outdir)/%.fits: $(res-i-indir)/%.fits | $(res-i-outdir)
	swarp $< -c reproduce/analysis/astromatic/default.swarp -IMAGEOUT_NAME $@ 





# G directories
res-g-indir = $(BDIR)/g-astrometry
res-g-outdir = $(BDIR)/g-resampled


# Resampled images G-filter
# -------------------------
#
# Pattern match for the resampled images. 
res-g-images = $(foreach resg,$(base-names),$(res-g-outdir)/$(resg).fits)

# Resample R filter and build the output directory.
# The default.swarp file should be modified : you can choose manually (CENTER TYPE = MANUAL), 
# the coordinates to resample and the dimension of the output images . 
# In this case should be 4084 4484.
# The rule to create the resampled images uses swarp with the astrometrized images.
$(res-g-outdir):; mkdir $@
$(res-g-images): $(res-g-outdir)/%.fits: $(res-g-indir)/%.fits | $(res-g-outdir)
	swarp $< -c reproduce/analysis/astromatic/default.swarp -IMAGEOUT_NAME $@ 





# Z directories
res-z-indir = $(BDIR)/z-astrometry
res-z-outdir = $(BDIR)/z-resampled


# Resampled images Z-filter
# -------------------------
#
# Pattern match for the resampled images. 
res-z-images = $(foreach resz,$(base-names),$(res-z-outdir)/$(resz).fits)

# Resample Z filter and build the output directory.
# The default.swarp file should be modified : you can choose manually (CENTER TYPE = MANUAL), 
# the coordinates to resample and the dimension of the output images . 
# In this case should be 4084 4484.
# The rule to create the resampled images uses swarp with the astrometrized images.
$(res-z-outdir):; mkdir $@
$(res-z-images): $(res-z-outdir)/%.fits: $(res-z-indir)/%.fits | $(res-z-outdir)
	swarp $< -c reproduce/analysis/astromatic/default.swarp -IMAGEOUT_NAME $@ 





# U directories
res-u-indir = $(BDIR)/u-astrometry
res-u-outdir = $(BDIR)/u-resampled


# Resampled images U-filter
# -------------------------
#
# Pattern match for the resampled images. 
res-u-images = $(foreach resu,$(base-names),$(res-u-outdir)/$(resu).fits)

# Resample U filter and build the output directory.
# The default.swarp file should be modified : you can choose manually (CENTER TYPE = MANUAL), 
# the coordinates to resample and the dimension of the output images . 
# In this case should be 4084 4484.
# The rule to create the resampled images uses swarp with the astrometrized images.
$(res-u-outdir):; mkdir $@
$(res-u-images): $(res-u-outdir)/%.fits: $(res-u-indir)/%.fits | $(res-u-outdir)
	swarp $< -c reproduce/analysis/astromatic/default.swarp -IMAGEOUT_NAME $@ 






# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#$(sw-outdir)/coadd-r-filter.fits $(sw-outdir)/coadd-i-filter.fits $(sw-outdir)/coadd-z-filter.fits

$(mtexdir)/resample.tex:  $(res-g-images)  
	touch $@




