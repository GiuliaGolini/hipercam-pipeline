# Download catalogue
#
# This Makefile is written to download the sdss stripe 82 catalog to do astrometry.
# The main goal is to have a template to download a star catalog. 
# The script calls a Python routine which allows to decide the area of sky to search for in a catalog.
# The routine also need also an archieve of configuration "inputcatalog.mk" where
# the coordinates, the area dimension and the name of the catalog are set.
# Moreover this script builds the indexes needed to do the astrometry of images.
# http://astrometry.net/doc/build-index.html
# 
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Define the directory where the buid image is going to be
dc-outdir = $(BDIR)/download-catalog





# This is the name of the galaxy to study to add at the catalog name
ASTR-OBJECTS = UGC00180





# Downloading the catalogue
# -------------------------
#
# Create the output directory and the star-catalog calling python routine.
# The prerequisite is the inputcatalog.mk file which can be modified. 
$(dc-outdir): ; mkdir $@
$(dc-outdir)/$(ASTR-OBJECTS)-star-catalogue.fits: reproduce/analysis/config/inputcatalog.mk | $(dc-outdir)
	# Call the python routine 
	python3 reproduce/analysis/python/download-catalog.py $(catstripe) $@





# Build the indexes
# -----------------
#
# Here define a function to build the indexes needed for the routine " solve-field" to do astrometry,
# three arguments:
# Target: index name
# Prerequisit: catalogue with all stars previously downloaded
# Index number: number for making the index
# -A defines the column of right ascension
# -D defines the column of declination
# -S defines the column of g magnitutes
# -E is used because my catalog only covers a small part of the sky
define BINDEX
$(1): $(2)
	build-astrometry-index -i $$< -e1 -o $$@ -P $$$(3) -S gmag -E -A RA_ICRS -D DE_ICRS
endef





# Define the index file names.
# First loop is needed to build the catalog.
# Second loop is for make indexes from -5 to 1, they correspond to
# different sizes on the sky, from smaller to bigger.
# For example 0 should be good for images about 6 arcmin in size and 
# it goes down and up in steps of sqrt(2).
bci-indexes = $(foreach bci-catalogue, $(dc-outdir)/$(ASTR-OBJECTS)-star-catalogue.fits,       \
               $(foreach bci-i, $(shell seq -5 1),                                             \
                $(subst .fits,_$(bci-i).index,$(bci-catalogue))  ))
# $(info $(bci-indexes))
# exit 1





# Build the indexes by calling the function inside two loops:
# Foreach object and foreach index number, generate the index.
# The function has three arguments:
# Target: index name
# Prerequisit: catalogue with all stars previously downloaded
# Index number: number for making the index
$(foreach bci-obj, $(ASTR-OBJECTS),                                                 \
 $(foreach bci-i, $(shell seq -5 1),                                                \
  $(eval                                                                            \
   $(call BINDEX,                                                                   \
    $(subst .fits,_$(bci-i).index,$(dc-outdir)/$(bci-obj)-star-catalogue.fits),     \
    $(dc-outdir)/$(bci-obj)-star-catalogue.fits,                                    \
    $(bci-i)                                                                     ))))





# Save the configuration file for solve-field
# --------------------------------------------
#
# This configuration file is the final target of the current script.
# It is the configuration file that `solve-field' will read when doing the
# astrometry of the images. It is specified the following information:
#     Run in parallel
#     Time limit for using the
#     Path where the indexes are
#     Use autoindex
$(dc-outdir)/astrometry.cfg: $(bci-indexes)
	# New lines introduced by \n
	echo -e "inparallel\ncpulimit 300\nadd_path $(dc-outdir)\nautoindex" > $@






# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when the configuration file has been
# generated.
$(mtexdir)/download-catalog.tex: $(dc-outdir)/astrometry.cfg 
	touch $@