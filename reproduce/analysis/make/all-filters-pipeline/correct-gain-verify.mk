# Gain correction with sigma clip mean coefficients.
#
# This Makefile is written to correct all extensions present in a HiPERCAM image for the gain error:
# HiPERCAM gain is 1.2 elect/adu.
# The main goal is to have a template to correct the gain for each filter images. 
# To do that, a loop for reading all extensions is used.
# The script calls a Python routine which is written to build the filter images (by combining the 
# corresponding four window)
# and correct of gain error by using the sigma clip mean values of the gain correction coefficient in the 
# previous step.
# Then the makefile creates cube images containing the CCD's images corrected for each block of observations.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid images are going to be
cgv-outdir = $(BDIR)/corrected-gain-verified





# Define the directory where the corrected for bias images from HiPERCAM are taken
cgv-indir = $(BDIR)/corrected-flat2






# Now defining a sub-target. To create the final text file we have to use the images corrected.
# This works as a loop: foreach input in the set of basenames writes in the outdirectory the name of the final image, 
# the same name as the input image but in another directory.
cgain-images-verified = $(foreach cg-input,$(base-names),$(cgv-outdir)/$(cg-input).fits)




# Correct the images of gain
# --------------------------
#
# Use cgain-images-verified as variables.
# To create the corrected images we need the ones taken from indirectory.
# Calling the python routine, with the mean values of coefficients from file median.txt
# previously build and the number of columns needed to correct the U filter.
$(cgv-outdir): ; mkdir $@
$(cgain-images-verified): $(cgv-outdir)/%.fits: $(cgv-indir)/%.fits | $(cgv-outdir)
	# Call the python routine 
	python3 reproduce/analysis/python/joining-verified.py $< $@ $(BDIR)/corrected-gain/sigclip-means.txt 3






# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-gain-verify.tex: $(cgain-images-verified)
	touch $@


