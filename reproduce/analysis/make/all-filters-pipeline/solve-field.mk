# Make the astrometry for U filter
#
# This Makefile is written to do the astrometry of U G R I Z filters image using 
# solve-field routine.
# The main goal is to have a template to select the images and add the astrometry.
# This website recall how solve field works:
# https://www.mankier.com/1/solve-field
#
# The problem in the U filter's images is that we can detect only few stars in 
# each image (less than 10 sometime). With those few stars solve-field cannot build 
# a correct catalog to make astrometry.
# First solution was to lower the odds to solve level (which is 1e6 default). Not working.
# By lowing the code tolerance the astrometry is not correct. Some images are not placed
# in the right position of the sky.
# A second solution could be to use the center of each astrometrized image in G filter and
# give the values of RA and DEC of the center ( with the radius around the center
# where to looking for) to solve field to run the astrometry.
# This ca be done by defining the rule to create the text file of each image with the right 
# ascension coordinate and declination.
# This is done by using astfits $< -h1 |grep CRVAL1 (CRVAL2) > $@.
# then giving to solve field the parameters --ra --dec with aststatistics $(word 3, $^) -c3 --mean)
# and the --radius where look for.
# This is not working so good anyway. The stars are too few.
# The option we use is to build a moffattian function (kernel.fits) and apply it to each image in
# U band. Meaning do a spatial convolution . The final image is smoothed, and we can see more sources.
# Finally we apply solve field to the convolved images to make the astrometry. The astrometry is finally 
# sent to the origina (not convolved) images.
# Since we have this problem we use this guideline also to do astrometry on the others filters images.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Kernel Function 
#----------------
#
# Define the rule to make the kernel file to do smoothing: 
# we use a molfattian function for the convolution
k-outdir = $(BDIR)/kernel
$(k-outdir):; mkdir $@
$(k-outdir)/kernel.fits : | $(k-outdir)
	astmkprof --kernel=moffat,3,2.8,5 --oversample=1 --output=$@ 





# Define the rule to make the catalog for astrometry valid for all the filters
ca-astrometry-cfg = $(BDIR)/download-catalog/astrometry.cfg



# U-FILTER
# -----------
#
# Define the directory with the u filter images
u-indir = $(BDIR)/u-filter
# Define the directory where the buid u images astrometrized
u-outdir = $(BDIR)/u-astrometry




$(u-outdir):; mkdir $@
u-astr-ims = $(foreach uname, $(base-names), $(u-outdir)/$(uname).fits)
$(u-astr-ims): $(u-outdir)/%.fits: $(u-indir)/%.fits $(k-outdir)/kernel.fits | $(u-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                  \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 1e3                        \
                --dir $(u-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)         

	#--sextractor-config /Users/princess/final/Hipercam_pipeline/reproduce/analysis/astromatic/default.sex \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@    # da cambiare!!!
	
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)






# G-FILTER
# -----------
#
# Define the directory with the g filter images
g-indir = $(BDIR)/g-filter
# Define the directory where the buid g images astrometrized
g-outdir = $(BDIR)/g-astrometry



$(g-outdir):; mkdir $@
g-astr-ims = $(foreach uname, $(base-names), $(g-outdir)/$(uname).fits)

$(g-astr-ims): $(g-outdir)/%.fits: $(g-indir)/%.fits $(k-outdir)/kernel.fits | $(g-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                  \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
                --dir $(g-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)         

	#--sextractor-config /Users/princess/final/Hipercam_pipeline/reproduce/analysis/astromatic/default.sex \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@    # da cambiare!!!
	
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)



#-------------------------------------------------------------------------------
# R-FILTER
# --------
#
# Define the directory with the r filter images
r-indir = $(BDIR)/r-filter
# Define the directory where the buid r images astrometrized
r-outdir = $(BDIR)/r-astrometry


# Obtain the astrometry with `solve-field' for R filter
# -----------------------------------------------------
#

$(r-outdir):; mkdir $@
r-astr-ims = $(foreach rname, $(base-names), $(r-outdir)/$(rname).fits)
$(r-astr-ims): $(r-outdir)/%.fits: $(r-indir)/%.fits $(k-outdir)/kernel.fits | $(r-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                  \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 1e4                        \
                --dir $(r-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)         

	#--sextractor-config /Users/princess/final/Hipercam_pipeline/reproduce/analysis/astromatic/default.sex \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@    # da cambiare!!!
	
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)










#-------------------------------------------------------------------------------
# I-FILTER
# -----------
#
# Define the directory with the i filter images
i-indir = $(BDIR)/i-filter
# Define the directory where the buid i images astrometrized
i-outdir = $(BDIR)/i-astrometry



# Define the rule to create the smoothed images 
conv-i-outdir = $(BDIR)/convolved-im-i

$(conv-i-outdir):; mkdir $@
conv-i-images = $(foreach inp,$(base-names),$(conv-i-outdir)/$(inp).fits)
$(conv-i-images): $(conv-i-outdir)/%.fits : $(i-indir)/%.fits $(k-outdir)/kernel.fits | $(conv-i-outdir)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$@ 




# Obtain the astrometry with `solve-field' for I filter
# -----------------------------------------------------
#

$(i-outdir):; mkdir $@
i-astr-ims = $(foreach iname, $(base-names), $(i-outdir)/$(iname).fits)
$(i-astr-ims): $(i-outdir)/%.fits: $(i-indir)/%.fits $(k-outdir)/kernel.fits | $(i-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --dir $(i-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          

	#--odds-to-solve 10                         \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)





#-------------------------------------------------------------------------------
# Z-FILTER
# -----------
#
# Define the directory with the z filter images
z-indir = $(BDIR)/z-filter
# Define the directory where the buid z images astrometrized
z-outdir = $(BDIR)/z-astrometry





$(z-outdir):; mkdir $@
z-astr-ims = $(foreach zname, $(base-names), $(z-outdir)/$(zname).fits)
$(z-astr-ims): $(z-outdir)/%.fits: $(z-indir)/%.fits $(k-outdir)/kernel.fits | $(z-outdir)
	temp=$(subst .fits,_c.fits,$@)
	astconvolve $< --kernel=$(word 2,$^) --type=float32 --domain=spatial --khdu=1 --output=$$temp
	solve-field $$temp                                     \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --dir $(z-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@) 

		        #--odds-to-solve 0.7                        \         


	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,_c.fits,$@)
	rm $(subst .fits,_c.axy,$@)
	rm $(subst .fits,_c.corr,$@)
	rm $(subst .fits,_c-indx.xyls,$@)
	rm $(subst .fits,_c.match,$@)
	rm $(subst .fits,_c.new,$@)
	rm $(subst .fits,_c.rdls,$@)
	rm $(subst .fits,_c.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)






# Only one rule, multiple targets
#
#


# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
 #$(i-astr-ims) $(z-astr-ims)  $(u-astr-ims)
#
$(mtexdir)/solve-field.tex: $(g-astr-ims) $(r-astr-ims)  $(i-astr-ims)  
	touch $@


