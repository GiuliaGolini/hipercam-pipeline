# Correction for the second version of flat
#
# This Makefile is written to correct all extensions present in a HiPERCAM images for the second version of flat error .
# The main goal is to have a template to make the flat correction in each of the four window in every CCD cameras of HiPERCAM.
# The correction is done by dividing each image extension for the corresponding second flat.
# To do that, a loop for reading all extensions is used.
# The makefile create cube images containing the corrected images for each block of observations.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Define the directory where the buid images are going to be
cf2-outdir = $(BDIR)/corrected-flat2





# Define the directory where the corrected for bias images from HiPERCAM are taken
cf2-indir = $(BDIR)/corrected-bias





# Now defining a sub-target. To create the final text file we have to use the images corrected of second flat.
# This is a loop foreach input in the set of basenames to do the patternmatch of cflat2-images.
# They will have the same name of the input ones.
cflat2-images = $(foreach cf2-input,$(base-names),$(cf2-outdir)/$(cf2-input).fits)





# Correct the image for the second version of flat
# ------------------------------------------------
#
# We use cflat2-images as variables.
# To create the corrected images we need the bias corrected ones taken from indirectory and the second flat one.
# Create also the output directory.
$(cf2-outdir): ; mkdir $@
$(cflat2-images): $(cf2-outdir)/%.fits: $(cf2-indir)/%.fits $(BDIR)/flat2/flats2.fits | $(cf2-outdir)
	# The 3 following steps are needed to create images with the same raw images
	# format: containing 20 sub-images corresponding to 4 window
	# each filter (us gs  rs is zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# flat data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image corrected
	# Correction of each image by dividing itself by the flat image.
	# It is important to associate the correct hdu for each image.
	# Inject the corrected data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                                     \
	do                                                                         \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                       \
	&& astarithmetic $< --hdu=$$n $(word 2, $^) --hdu=$$n / --output=$$tmpim   \
	&& astfits $$tmpim --copy=1 --output=$@                                    \
	&& rm $$tmpim                                                              \
	; done





# TeX macros final target
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-flat2.tex: $(cflat2-images)
	touch $@
