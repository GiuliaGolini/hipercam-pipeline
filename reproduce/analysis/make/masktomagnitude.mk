# Magnitude of the galaxy
#
# Take the mask of the deep image of the galaxy
# Do the deep image ( stacking of the three filters)
# astarithmetic r-stacked-final/stacked-r-filter.fits g-stacked-final/stacked-g-filter.fits i-stacked-final/stacked-i-filter.fits -g1 3 mean
# Crop the deep image to remove the noise that could make noisecheisel not work well
# astcrop deep.fits --mode=img --polygon=1473,1586:2618,1574:2600,3549:1386,3338 -ocropdeep.fits
# Mask the deep image 

# Play with the parameter tilesize to make a better sky (look at the mask image)



# astnoisechisel cropdeep.fits --til
# Make a kernel function to improve the detection
# astmkprof --kernel=gaussian,5,3 --oversample=1
# Use Segment to break the detection image into objects.
# astsegment cropdeep_detected.fits --kernel=kernel.fits

#i="cropdeep_detected_segmented.fits -h1"
#clumps="cropdeep_detected_segmented.fits -h2"

# Doing the object masked
# astarithmetic $i $clumps 0 gt nan where -omasked.fits

# With this we make a table catalog
# astmkcatalog cropdeep_detected_segmented.fits --valuesfile=masked.fits \
#--valueshdu=1 --ids --brightness --magnitude --zeropoint=22.5
# See the magnitude only of the object!(305)
#asttable cropdeep_detected_segmented_cat.fits |grep ^305

#better segmentation
#astsegment cropdeep_detected.fits --kernel=kernel.fits --gthresh=0  --objbordersn=0.5
# one way---------

#
# Make the mask 
# Erode to delete the "rivers" between clumps
# set i sets i as a variable with the result of the previous expression
# astarithmetic $o $clumps 0 gt $o 298 ne or nan where set-i i i isblank 0 where 0 gt 2 erode -olabelmask.fits
# astmkcatalog labelmask.fits --hdu=1 --valuesfile=cropdeep_detected.fits --valueshdu=1 --ids --brightness --magnitude --zeropoint=22.5 
# asttable labelmask_cat.fits





#
# Original author:
# Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# The calibration factors will be written in a txt file in the outdirectory x-photometry.
# To build those factors we need the images with the astrometry and the catalog we used to do astrometry.
# Which has also the fotometry of the stars.
.ONESHELL:


all: /Users/princess/v-phot-UGC00180/deep/done.txt

/Users/princess/v-phot-UGC00180/deep/deepmask.fits: /Users/princess/v-phot-UGC00180/deep/deep.fits 
	# Define image names: detection, segmentation, catalogue, match
	# between segmentation and reference catalogue, difference 
	# Use Noisechisel to obtain the detection image.
	crop=$(subst .fits,-crop.fits,$<)
	det=$(subst .fits,-det.fits,$<)
	seg=$(subst .fits,-seg.fits,$<)
	fm=$(subst .fits,-fm.fits,$<)
	ftm=$(subst .fits,-ftruem.fits,$<)
	firstmasked=$(subst .fits,-first.fits,$<)
	secondmasked=$(subst .fits,-second.fits,$<)
	thirdmasked=$(subst .fits,-third.fits,$<)
	mask=$(subst .fits,--mask.fits,$<)

	#Crop the deep image to remove the noise that could make noisecheisel not work well
	astcrop $< --mode=img --polygon=1473,1586:2618,1574:2600,3549:1386,3338 --output=$$crop

	astnoisechisel $$crop --output=$$det # Play with --tilesize=25,25 to improve the sky detection of noisecheisel \
	#--config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astnoisechisel.conf \
	# Build the kernel to improve astsegment
	astmkprof --kernel=gaussian,5,3 --oversample=1 

	# Use Segment to break the detection image into objects.
	astsegment $$det --kernel=kernel.fits --gthresh=0 \
	--config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astsegment.conf \
               --output=$$seg \
	# Multiply  the image for clumps to see if the mask is well done (output as a image with nan instead of clumps)
	astarithmetic $$seg --hdu=1 $$seg --hdu=clumps 0 gt nan where --output=$$firstmasked

	# With astmkcatalog build a table with the magnitude of the objects of the segmented image. 
	astmkcatalog $$seg --valuesfile=$$firstmasked --valueshdu=1 --ids --brightness --magnitude --zeropoint=22.5  
	# choose the 298 to visualize only the magnitude of the galaxy ( which has the value 298)
	# asttable cropdeep_detected_segmented_cat.fits |grep ^298

	# Build the mask to use for all the filters
	# 
	astarithmetic $$seg --hdu=OBJECTS $$se --hdu=CLUMPS 0 gt $$seg --hdu=OBJECTS 298 ne or nan where set-i i i \
	isblank 0 where 0 gt 2 erode --output=$$mask


		
/Users/princess/v-phot-UGC00180/deep/done.txt : /Users/princess/v-phot-UGC00180/deep/deepmask.fits
	echo $^ >  $@





