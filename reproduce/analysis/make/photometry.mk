# Photometric calibration of each image
#
# This Makefile is written to do the photometric calibration of each image.
# With this makefile read the astrometrized images and multiply it
# for the correspondinf calibration factor calculated in the previous step. 
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):
# Copyright (C) 2019, Giulia Golini
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.






# THIS WORKS ONLY FOR A GIVEN FILTER , CHANGE THE DIRECTORY ACCORDING TO THE FILTER YOU ARE WORKING WITH

# Directories
# -----------
#
p-indir  = $(BDIR)/g-astrometry
p-outdir = $(BDIR)/g-photometry-done
fact-indir = $(BDIR)/g-photometry

# Now the pipeline is not automatic and I am doing it step by step
# because I want to do it one filter per time.
# I have to call the input names each time.
# Later, when automatic, this step can be deleted and the  
# makefile will read straightly the basenames.
# ---------------------------------------------------------------------------------
cg-inputs = $(wildcard $(p-indir)/*.fits)
base-names = $(foreach cb-input,$(cg-inputs),$(notdir $(subst .fits,,$(cb-input))))
# ---------------------------------------------------------------------------------



# To build the photometrized images we need the astrometrized ones and the 
# correction factors.

phot-images  = $(foreach bname,$(base-names),$(p-outdir)/$(bname).fits)
$(p-outdir): ; mkdir $@
$(phot-images): $(p-outdir)/%.fits: $(p-indir)/%.fits $(fact-indir)/%.txt | $(p-outdir)
	factor=$$(awk '{print $$1}' $(word 2, $^)) \
	&& astarithmetic $(word 1, $^) --hdu=1 $$factor x --output=$@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all photometric calibration
# factors have been obtained.
$(mtexdir)/photometry.tex: $(phot-images) 
	touch $@


