# Stacking of the images
#
# This Makefile is written to coadd all the photometrized images of each filter.
# This is done by using a sigma clip mean between all the images resampled with nans.
# This routine is done manually , step by step because swarp doesn't use sigmaclipping
# to coadd images. Sigma clip mean is a stronger parameter when coadding images instead of median.
# I have written a rule for each filter. This can improved later by using only
# one rule for differrent targets.
#
# NOTE:
# with the rule
# astarithmetic $^ -g1 $(words $^) number --output=$@
# you can build an image which counts how many images are 
# added in the pixeles ( pixel value = number of images added).
# In this way you can know for how much time the source is really been observed.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Now the pipeline is not automatic and I am doing it step by step
# because I want to do it one filter per time.
# I have to call the input names each time.
# Later, when automatic, this step can be deleted and the  
# makefile will read straightly the basenames.
# ---------------------------------------------------------------------------------
# CHANGE THE INPUT DIRECTORY WITH THE FILTER YOU ARE REDUCING
cg-inputs = $(wildcard $(stack-g-indir)/*.fits)
base-names = $(foreach cb-input,$(cg-inputs),$(notdir $(subst .fits,,$(cb-input))))
# ---------------------------------------------------------------------------------




# R-BAND

# R directories
stack-r-indir = $(BDIR)/r-nan-resampled
stack-r-outdir = $(BDIR)/r-stacked

# Stacking images R-filter
# ------------------------
#
# Defining the "Nan" resampled input images. 
r-stack-inputs = $(foreach input,$(base-names),$(stack-r-indir)/$(input).fits)

# Stacking R filter and build the output directory by using sigmaclipping.
$(stack-r-outdir):; mkdir $@
$(stack-r-outdir)/stacked-r-filter.fits: $(r-stack-inputs)| $(stack-r-outdir)
	astarithmetic $^ -g1 $(words $^) 3 0.2 sigclip-mean --output=$@





# I-BAND

# I directories
stack-i-indir = $(BDIR)/i-nan-resampled
stack-i-outdir = $(BDIR)/i-stacked

# Stacking images I-filter
# ------------------------
#
# Defining the "Nan" resampled input images. 

i-stack-inputs = $(foreach inputt,$(base-names),$(stack-i-indir)/$(inputt).fits)

# Stacking I filter and build the output directory.
$(stack-i-outdir):; mkdir $@
$(stack-i-outdir)/stacked-i-filter.fits: $(i-stack-inputs)| $(stack-i-outdir)
	astarithmetic $^ -g1 $(words $^) 3 0.2 sigclip-mean --output=$@





# G-BAND

# G directories
stack-g-indir = $(BDIR)/g-nan-resampled
stack-g-outdir = $(BDIR)/g-stacked


# Stacking images G-filter
# ------------------------
#
# Defining the "Nan" resampled input images. 
g-stack-inputs = $(foreach inpu,$(base-names),$(stack-g-indir)/$(inpu).fits)

# Stacking G filter and build the output directory.
$(stack-g-outdir):; mkdir $@
$(stack-g-outdir)/stacked-g-filter.fits: $(g-stack-inputs)| $(stack-g-outdir)
	astarithmetic $^ -g1 $(words $^) 3 0.2 sigclip-mean --output=$@





# Z-BAND

# Z directories
stack-z-indir = $(BDIR)/z-nan-resampled
stack-z-outdir = $(BDIR)/z-stacked


# Stacking images Z-filter
# ------------------------
#
# Defining the "Nan" resampled input images. 
z-stack-inputs = $(foreach inp,$(base-names),$(stack-z-indir)/$(inp).fits)

# Stacking Z filter and build the output directory.
$(stack-z-outdir):; mkdir $@
$(stack-z-outdir)/stacked-z-filter.fits: $(z-stack-inputs)| $(stack-z-outdir)
	astarithmetic $^ -g1 $(words $^) 3 0.2 sigclip-mean --output=$@





# U-BAND

# U directories
stack-u-indir = $(BDIR)/u-nan-resampled
stack-u-outdir = $(BDIR)/u-stacked


# Stacking images U-filter
# ------------------------
#
# Defining the "Nan" resampled input images. 
u-stack-inputs = $(foreach input,$(base-names),$(stack-u-indir)/$(input).fits)

# Stacking U filter and build the output directory.
$(stack-u-outdir):; mkdir $@
$(stack-u-outdir)/stacked-u-filter.fits: $(u-stack-inputs) | $(stack-u-outdir)
	astarithmetic $^ -g1 $(words $^) 3 0.2 sigclip-mean --output=$@





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
# $(stack-g-outdir)/stacked-g-filter.fits $(stack-r-outdir)/stacked-r-filter.fits

$(mtexdir)/stacking.tex: $(stack-g-outdir)/stacked-g-filter.fits  
	touch $@




