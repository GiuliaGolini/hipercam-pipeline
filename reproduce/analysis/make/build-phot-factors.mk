# Photometric calibration of each image
#
# This Makefile is written to do the photometric calibration of each image.
# To to that, we need an input catalog, with the photometry of the stars and 
# our astrometrized images.
# After that, obtain a catalogue for each image
# using `astnoisechisel, `astsegment, and `astmakecatalog'. Then a
# cross matching between the input catalogue and the catalogue of each image,
# selecting only the matched objects. For those objects, compare the
# difference in magnitudes and obtain a sigma clip mean of that difference.
# Then, the photometric calibration factor for each image is f=10^-0.4*mdif
#
# Original author:
# Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.


# THIS WORKS ONLY FOR A GIVEN FILTER , CHANGE THE DIRECTORY ACCORDING TO THE FILTER YOU ARE WORKING WITH

# Directories
# -----------
#
pc-indir  = $(BDIR)/g-astrometry
pc-outdir = $(BDIR)/g-photometry



# Obtain the calibration factors
# ------------------------------
#
# Here we define the names of the inputs images


# Now the pipeline is not automatic and I am doing it step by step
# because I want to do it one filter per time.
# I have to call the input names each time.
# Later, when automatic, this step can be deleted and the  
# makefile will read straightly the basenames.
# ---------------------------------------------------------------------------------
cg-inputs = $(wildcard $(pc-indir)/*.fits)
base-names = $(foreach cb-input,$(cg-inputs),$(notdir $(subst .fits,,$(cb-input))))


# The calibration factors will be written in a txt file in the outdirectory x-photometry.
# To build those factors we need the images with the astrometry and the catalog we used to do astrometry.
# Which has also the fotometry of the stars.
factors  = $(foreach bname,$(base-names),$(pc-outdir)/$(bname).txt)
$(pc-outdir): ; mkdir $@
$(factors): $(pc-outdir)/%.txt: $(pc-indir)/%.fits $(BDIR)/download-catalog/UGC00180-star-catalogue.fits | $(pc-outdir)
	# Define image names: detection, segmentation, catalogue, match
	# between segmentation and reference catalogue, difference in
	# magnitudes of the matched objects.
	im_det=$(subst .txt,-det.fits,$@)
	im_seg=$(subst .txt,-seg.fits,$@)
	im_cat=$(subst .txt,-cat.fits,$@)
	im_mat=$(subst .txt,-matched.fits,$@)
	im_dif=$(subst .txt,-magdiff.txt,$@)

	# Use Noisechisel to obtain the detection image.
	astnoisechisel $< --hdu=1 --output=$$im_det \
                   --config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astnoisechisel.conf \
                          

	# Use Segment to break the detection image into objects.
	astsegment $(subst .txt,-det.fits,$@) \
	--config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astsegment.conf \
               --output=$$im_seg \

	# Use Makecatalog to obtain a catalogue with properties of detected
	# objects. Compute: ra, dec, magnitude.
	astmkcatalog $(subst .txt,-seg.fits,$@) --ra --dec --magnitude --zeropoint=22.5 \
	--config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astmkcatalog.conf \
                     --output=$$im_cat 

	# Match between objects that are in the detected catalogue and in
	# the SDSS catalogue. Radius of search is 1 arcsec, take as output
	# all columns from both tables.
	astmatch $(word 2,$^) $(subst .txt,-cat.fits,$@) \
                 --ccol1=RA_ICRS,DE_ICRS --ccol2=RA,DEC --aperture=1.0/3600.0 \
                 --outcols=a_all,b_all \
                 --output=$$im_mat \
                 --config=/Users/princess/final/Hipercam_pipeline/reproduce/software/config/gnuastro/astmatch.conf 

	# Use Asttable to read the matched table, save
	# the difference between tese two magnitudes in a file:
	# Mag_catalog - Mag_mycatalog
	asttable $(subst .txt,-matched.fits,$@) -cgmag,MAGNITUDE \
                 | awk '{print $$1-$$2}' \
                 > $$im_dif

	# Compute the calibration factor as the sigma clipped mean of the
	# difference of the two magnitudes. As the difference is in
	# magnitudes, the factor (in flux) for which each image has to be
	# scaled is: factor = 10^(-0.4*m), where m=$$1 is the sigmaclip mean
	# of the  difference between the magnitude in the SDSS catalogue and
	# the magnitudes computed by I with Makecatalog.
	factor=$$(aststatistics $$im_dif -c1 --sigclip-mean | awk '{print 10^(-0.4*$$1)}')

	# Save in the target the base name and the photometric calibration
	# factor.
	echo " $$factor" > $@

	# Remove temporal images
	rm $$im_det
	rm $$im_seg
	rm $$im_cat
	rm $$im_mat
	rm $$im_dif




# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all photometric calibration
# factors have been obtained.
$(mtexdir)/build-phot-factors.tex: $(factors) 
	touch $@


