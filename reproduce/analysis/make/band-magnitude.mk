# Magnitude of the galaxy
#

#
# Original author:
# Raul Infante-Sainz <infantesainz@gmail.com>
# Contributing author(s): Giulia Golini  <giulia.golini@gmail.com>
# Copyright (C) 2019, Raul Infante-Sainz.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# The calibration factors will be written in a txt file in the outdirectory x-photometry.
# To build those factors we need the images with the astrometry and the catalog we used to do astrometry.
# Which has also the fotometry of the stars.
.ONESHELL:


all: /Users/princess/v-phot-UGC00180/g-stacked-phot/done.txt

/Users/princess/v-phot-UGC00180/g-stacked-phot/masked-stacked-g.fits: /Users/princess/v-phot-UGC00180/g-stacked-phot/stacked-g-filter.fits 
	# Define image names: detection, segmentation, catalogue, match
	# between segmentation and reference catalogue, difference 
	# Use Noisechisel to obtain the detection image.
	masked=$(subst .fits,-first.fits,$<)
	crop=$(subst .fits,-crop.fits,$<)
	astcrop $< --mode=img --polygon=1473,1586:2618,1574:2600,3549:1386,3338 --output=$$crop

	# Mask the image!!con la maschera appena creata il numero corrisponde al numero che ha posto la maschera alla galassia!!
	# Quando mascheri può essere che la maschera abbia blanck! controlla!--> output final true mask
	astarithmetic $$crop --hdu=1 /Users/princess/v-phot-UGC00180/g-stacked-phot/labelmask.fits --hdu=1 0 eq nan where --output=$$masked \
	# Masking the image with final true mask
	astmkcatalog /Users/princess/v-phot-UGC00180/g-stacked-phot/labelmask.fits --hdu=1 --valuesfile=$$crop --valueshdu=1 --ids --brightness --magnitude --zeropoint=22.5


# astmkcatalog labelmask.fits --hdu=1 --valuesfile=stacked-g-filter-crop.fits --valueshdu=1 --ids --brightness --magnitude --zeropoint=22.5
# asttable labelmask_cat.fits ---> visualizzi la magnitudine

		
/Users/princess/v-phot-UGC00180/g-stacked-phot/done.txt: /Users/princess/v-phot-UGC00180/g-stacked-phot/masked-stacked-g.fits
	echo $^ >  $@




