# Prepare bad raw images
#
# This make file is written to mask with nans the part of the image which is 
# "bad". This means when we had problems when observing and taking datas.
# In this case we have a problem with 2 blocks:
# One shows a bad left top corner in each image extension,
# the other shows a bad bottom left corner.
# To not lose depht, because the galaxy we are observing is very faint,
# we decided to use this images masking the bad part. If not masked the sky
# model we fit would not be correct resulting in an oversubtraction.
# This makefile calls a python script to mask half image with nans.
# This makefile should be used when needed.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid images are going to be
mbi-outdir = $(BDIR)/good-images




# Define the directory where the corrected images from HiPERCAM are taken
mbi-indir = $(BDIR)/corrected-gain-verified




# Now the pipeline is not automatic and I am doing it step by step
# because I want to do it one filter per time.
# I have to call the input names each time.
# Later, when automatic, this step can be deleted and the  
# makefile will read straightly the basenames.
# ---------------------------------------------------------------------------------
# CHANGE THE INPUT DIRECTORY WITH THE FILTER YOU ARE REDUCING
cg-inputs = $(wildcard $(mbi-indir)/*.fits)
base-names = $(foreach cb-input,$(cg-inputs),$(notdir $(subst .fits,,$(cb-input))))
# ---------------------------------------------------------------------------------




# Now defining a sub-target. To create the final text file we have to use the images corrected.
# This works as a loop: foreach input in the set of basenames writes in the outdirectory the name of the final image 
# ,the same name as the input image but in another directory.
good-images = $(foreach input,$(base-names),$(mbi-outdir)/$(input).fits)

$(info $(good-images))



# Mask images
# -----------
#
# Use good-images as variables.
# To create the good images we need the ones taken from indirectory.
# Build the outdirectory.
# Call the python routine to build the good image.
$(mbi-outdir): ; mkdir $@
$(good-images): $(mbi-outdir)/%.fits: $(mbi-indir)/%.fits | $(mbi-outdir)
	# Call the python routine 
	python3 reproduce/analysis/python/mask-bad-im.py $< $@ 300






# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/mask-bad-images.tex: $(good-images) 
	touch $@


