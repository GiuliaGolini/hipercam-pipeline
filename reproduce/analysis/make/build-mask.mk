# Build the mask
#
# This Makefile is written to build the mask for images taken from HiPERCAM instrument.
# The main goal is to have a template to make the mask for each of the four window in every CCD cameras of HiPERCAM.
# To do that, a loop for reading all extensions is used.
# The mask is done by using astnoisecheisel for each extension.
# Astnoisechisel creates an output (by taking a fits image as input) a cube image with 4 extension: the input with the sky subtracted,
# the mask of the image, the sky and the rms of the sky.
# The mask is an image containing the value one where the software has recognized a real source and a value 0 where there is only noise.
# Then the makefile create cube images containing the masks.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Define the directory where the corrected of flat images from HiPERCAM are taken
bm-indir = $(BDIR)/corrected-flat





# Define the directory where the buid images are going to be
bm-outdir = $(BDIR)/mask




# Define the patternmatch for the mask-images.
# This works as a loop: foreach bm-input in the
# set of basenames writes in the outdirectory the same name of the input image.
mask-images = $(foreach bm-input,$(base-names),$(bm-outdir)/$(bm-input).fits)





# Build the mask
# --------------
#
# Comment on pattern match: the simbol % means taking only one image (and not all) each time.
# Define the rule: to create mask images we need the corrected (for flat and bias) images.
# Create also the output directory for mask images.
$(bm-outdir): ; mkdir $@
$(mask-images): $(bm-outdir)/%.fits: $(bm-indir)/%.fits | $(bm-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image. It has created only the hdu 0, when doing the bugle I add the others
	# hdu
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# mask data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the mask image
	# Create the mask of each image by using astnoisecheisel.
	# Note that the mask is the second output of astnoisecheisel and it is the only important image needed.
	# Inject the mask data (tmpim, hdu=2) into the target
	# Remove the temporal image
	# So now add hdu to the cube frame and at the same time read each hdu in the prerequisite image.
	for n in $$(seq 1 20);                                   \
	do                                                       \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                     \
	&& astnoisechisel $< --hdu=$$n --output=$$tmpim          \
	&& astfits $$tmpim --copy=2 --output=$@                  \
	&& rm $$tmpim                                            \
	;done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-mask.tex: $(mask-images)
	touch $@

