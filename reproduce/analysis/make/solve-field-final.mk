# Make the astrometry
#
# This Makefile is written to do the astrometry of G, R, I, Z filters image using 
# solve-field routine.
# The main goal is to have a template to select the images and add the astrometry.
# This website recall how solve field works:
# https://www.mankier.com/1/solve-field
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Directories
# -----------
#




# G-FILTER
# -----------
#
# Define the directory with the g filter images
g-indir = $(BDIR)/g-filter
# Define the directory where the buid g images astrometrized
g-outdir = $(BDIR)/g-astrometry





# R-FILTER
# -----------
#
# Define the directory with the r filter images
r-indir = $(BDIR)/r-filter
# Define the directory where the buid r images astrometrized
r-outdir = $(BDIR)/r-astrometry





# I-FILTER
# -----------
#
# Define the directory with the i filter images
i-indir = $(BDIR)/i-filter
# Define the directory where the buid i images astrometrized
i-outdir = $(BDIR)/i-astrometry





# Z-FILTER
# -----------
#
# Define the directory with the z filter images
z-indir = $(BDIR)/z-filter
# Define the directory where the buid z images astrometrized
z-outdir = $(BDIR)/z-astrometry






# U-FILTER
# -----------
#
# Define the directory with the u filter images
u-indir = $(BDIR)/u-filter
# Define the directory where the buid u images astrometrized
u-outdir = $(BDIR)/u-astrometry




	

# Obtain the astrometry with `solve-field' for G filter
# -----------------------------------------------------
#
ca-astrometry-cfg = $(BDIR)/download-catalog/astrometry.cfg

$(g-outdir):; mkdir $@
g-astr-ims = $(foreach gname, $(base-names), $(g-outdir)/$(gname).fits)
$(g-astr-ims): $(g-outdir)/%.fits: $(g-indir)/%.fits | $(g-outdir)
	solve-field $<                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 5                          \
                --dir $(g-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          

	# ODD TO SOLVE TO 1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	#rm $(subst .fits,.axy,$@)
	rm $(subst .fits,.corr,$@)
	rm $(subst .fits,-indx.xyls,$@)
	rm $(subst .fits,.match,$@)
	rm $(subst .fits,.new,$@)
	rm $(subst .fits,.rdls,$@)
	rm $(subst .fits,.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)





# Obtain the astrometry with `solve-field' for R filter
# -----------------------------------------------------
#

$(r-outdir):; mkdir $@
r-astr-ims = $(foreach rname, $(base-names), $(r-outdir)/$(rname).fits)
$(r-astr-ims): $(r-outdir)/%.fits: $(r-indir)/%.fits | $(r-outdir)
	solve-field $<                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
                --dir $(r-outdir)                          \
                --odds-to-solve 8                          \
                --verbose                                  \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          


	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	#rm $(subst .fits,.axy,$@)
	rm $(subst .fits,.corr,$@)
	rm $(subst .fits,-indx.xyls,$@)
	rm $(subst .fits,.match,$@)
	rm $(subst .fits,.new,$@)
	rm $(subst .fits,.rdls,$@)
	rm $(subst .fits,.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)





# Obtain the astrometry with `solve-field' for I filter
# -----------------------------------------------------
#

$(i-outdir):; mkdir $@
i-astr-ims = $(foreach iname, $(base-names), $(i-outdir)/$(iname).fits)
$(i-astr-ims): $(i-outdir)/%.fits: $(i-indir)/%.fits | $(i-outdir)
	solve-field $<                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 10                         \
                --dir $(i-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          


	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,.axy,$@)
	rm $(subst .fits,.corr,$@)
	rm $(subst .fits,-indx.xyls,$@)
	rm $(subst .fits,.match,$@)
	rm $(subst .fits,.new,$@)
	rm $(subst .fits,.rdls,$@)
	rm $(subst .fits,.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)





# Obtain the astrometry with `solve-field' for Z filter
# -----------------------------------------------------
#

$(z-outdir):; mkdir $@
z-astr-ims = $(foreach zname, $(base-names), $(z-outdir)/$(zname).fits)
$(z-astr-ims): $(z-outdir)/%.fits: $(z-indir)/%.fits | $(z-outdir)
	solve-field $<                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
                --use-sextractor                           \
		        --scale-low 0.07                           \
		        --scale-high 0.09                          \
		        --odds-to-solve 0.7                        \
                --dir $(z-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          


	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,.axy,$@)
	rm $(subst .fits,.corr,$@)
	rm $(subst .fits,-indx.xyls,$@)
	rm $(subst .fits,.match,$@)
	rm $(subst .fits,.new,$@)
	rm $(subst .fits,.rdls,$@)
	rm $(subst .fits,.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)






# Obtain the astrometry with `solve-field' for U filter
# -----------------------------------------------------
#

# Build the catalog with sextractor
#ucat-outdir =  $(BDIR)/ucat-filter
#$(ucat-outdir):; mkdir $@
#u-cat-astr-ims = $(foreach catname, $(base-names), $(ucat-outdir)/$(catname).fits)
#$(u-cat-astr-ims): $(ucat-outdir)/%.fits: $(u-indir)/%.fits | $(ucat-outdir)
	#sex $< \
   # -d /Users/princess/final/Hipercam_outputs/software/installed/share/sextractor/default.sex  \
  #  -output=$@





$(u-outdir):; mkdir $@
u-astr-ims = $(foreach uname, $(base-names), $(u-outdir)/$(uname).fits)
$(u-astr-ims): $(u-outdir)/%.fits: $(u-indir)/%.fits | $(u-outdir)
	solve-field $<                                         \
				--no-plots                                 \
                --overwrite                                \
                --extension 1                              \
		        --scale-low 0.05                           \
		        --scale-high 0.09                          \
		        --odds-to-solve  0.7                       \
		        --use-sextractor                           \
		        --sextractor-config /Users/princess/final/Hipercam_outputs/software/installed/share/sextractor/default.sex \
		        --x-column X_IMAGE --y-column Y_IMAGE \
		        --sort-column MAG_AUTO   \
		        --sort-ascending  \
		        --verbose \
                --dir $(u-outdir)                          \
		        --scale-units arcsecperpix                 \
		        --config $(ca-astrometry-cfg)              \
		        --wcs $(subst .fits,_wcs.fits,$@)          


	#--code-tolerance 0.017  \
	#--use-sextractor                           \
	#--sextractor-config /Users/princess/final/Hipercam_outputs/software/installed/share/sextractor/default.sex                    \
	#--odds-to-tune-up 1e5                         \
	#--sextractor-config /Users/princess/final/Hipercam_pipeline/reproduce/analysis/astromatic/default.sex \
	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
	rm $(subst .fits,.axy,$@)
	rm $(subst .fits,.corr,$@)
	rm $(subst .fits,-indx.xyls,$@)
	rm $(subst .fits,.match,$@)
	rm $(subst .fits,.new,$@)
	rm $(subst .fits,.rdls,$@)
	rm $(subst .fits,.solved,$@)
	rm $(subst .fits,_wcs.fits,$@)


# Obtain the astrometry with `solve-field'
# ----------------------------------------
#
# Pattern match, rule to build the output directory and 
# define the rule to create astrometrized images . The prerequisites are the filters images.
# Sove field routine needs an input catalog "ca-astrometry-cfg" previously built with the download catalog routine.
# scale low and scale high parameters are adapted to the Hipercam pixels dimensions.
# Define where is the catalog needed.
#ca-astrometry-cfg = $(BDIR)/download-catalog/astrometry.cfg

#$(ca-outdir):; mkdir $@
#ca-castr-ims = $(foreach ca-bname, $(base-names), $(ca-outdir)/$(ca-bname).fits)
#$(info $(ca-castr-ims))
#exit 1
#$(ca-castr-ims): $(ca-outdir)/%.fits: $(ca-indir)/%.fits | $(ca-outdir)
#	solve-field $<                                         \
#				--no-plots                                 \
#                --overwrite                                \
#                --extension 1                              \
#                --use-sextractor                           \
#		        --scale-low 0.07                           \
#		        --scale-high 0.09                          \
#                --dir $(ca-outdir)                         \
#		        --scale-units arcsecperpix                 \
#		        --config $(ca-astrometry-cfg)              \
#		        --wcs $(subst .fits,_wcs.fits,$@)          


	# Inject the wcs header into the actual .fits data image
	# NOTICE: when I did this step, it did not work, that is why the
	# python script header.py is used in the next step. CHECK THIS ISSUE
#	astarithmetic $< --wcsfile=$(subst .fits,_wcs.fits,$@) --wcshdu=0 --output=$@
	
	# Delete temporal files created by `solve-field'
#	rm $(subst .fits,.axy,$@)
#	rm $(subst .fits,.corr,$@)
#	rm $(subst .fits,-indx.xyls,$@)
#	rm $(subst .fits,.match,$@)
#	rm $(subst .fits,.new,$@)
#	rm $(subst .fits,.rdls,$@)
#	rm $(subst .fits,.solved,$@)
#	rm $(subst .fits,_wcs.fits,$@)





# Final TeX macro
# ---------------
#
# Make an empty .tex file as final file when all light images
# bias, dark and gain corrected have been obtained
#
$(mtexdir)/solve-field-final.tex: $(g-astr-ims)  
	touch $@


