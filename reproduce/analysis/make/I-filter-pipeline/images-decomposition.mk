# Images decomposition
#
# This makefile is written to divide the raw images in each filter.
# To do this we build fits images with 4 extensions.
# A fits image of 4 extension for each filter. This allows to work
# only with one filter per time.
# With this script we decompose all the bias images and all the raw images
# in the input directory.
# From hdu1 to hdu4 we have the U filter, 
# from hdu5 to hdu8 the G filter,
# from hdu9 to hdu12 the R filter,
# from hdu13 to hdu16 the I filter,
# from hdu17 to hdu20 the Z filter,
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Output directories where the buid images are going to be.
# Bias directories
bias-u = $(BDIR)/u-filter-bias
bias-g = $(BDIR)/g-filter-bias
bias-r = $(BDIR)/r-filter-bias
bias-i = $(BDIR)/i-filter-bias
bias-z = $(BDIR)/z-filter-bias

# Raw images directories
dec-u = $(BDIR)/u-filter
dec-g = $(BDIR)/g-filter
dec-r = $(BDIR)/r-filter
dec-i = $(BDIR)/i-filter
dec-z = $(BDIR)/z-filter



# Input directory of the bias images from HiPERCAM
im-bias-indir = $(INDIR)/bias
# Input directory of the raw images from HiPERCAM

im-indir = $(INDIR)/objects


z-filter-bias-images = $(foreach name, $(names), $(bias-z)/$(name))

$(bias-z):; mkdir $@
$(z-filter-bias-images): $(bias-z)/%.fits: $(im-bias-indir)/%.fits | $(bias-z)
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1
	for n in $$(seq 17 20); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 



# Decompose only U filter
#------------------------
#
# First we define a rule to build the bias images of U
# Then we build the raw images of U filter.
$(bias-u):; mkdir $@
u-filter-bias-images = $(foreach name, $(names), $(bias-u)/$(name))
$(u-filter-bias-images): $(bias-u)/%.fits: $(im-bias-indir)/%.fits | $(bias-u)
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1
	for n in $$(seq 1 4); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 


$(dec-u):; mkdir $@
u-filter-images = $(foreach name, $(base-names), $(dec-u)/$(name).txt)
$(u-filter-images): $(dec-u)/%.txt: $(ca-indir)/%.fits | $(dec-u)
	tmpim=$(subst .txt,.fits,$@)               \
	&& astfits $< --copy=1 --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@    \
	# Inject the prerequisite into the target
	echo $< > $@



g-filter-bias-images = $(foreach name, $(names), $(dec-g)/$(name))
$(bias-g):; mkdir $@
$(g-filter-bias-images): $(bias-g)/%.fits: $(im-bias-indir)/%.fits | $(bias-g)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 5 8); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 





r-filter-bias-images = $(foreach name, $(names), $(bias-r)/$(name))
$(bias-r):; mkdir $@
$(r-filter-bias-images): $(bias-r)/%.fits: $(im-bias-indir)/%.fits | $(bias-r)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 9 12); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 





i-filter-bias-images = $(foreach name, $(base-names), $(bias-i)/$(name).fits)
$(bias-i):; mkdir $@
$(i-filter-bias-images): $(dec-i)/%.fits: $(im-bias-indir)/%.fits | $(bias-i)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1
	for n in $$(seq 13 16); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 








$(mtexdir)/filter-decomposition.tex:  $(i-filter-images) | $(mtexdir)
	touch $@




