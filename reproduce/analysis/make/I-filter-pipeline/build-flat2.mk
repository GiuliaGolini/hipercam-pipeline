# Build the second version of the flat
#
# This Makefile is written to build the second flat for the raw images taken from HiPERCAM instrument.
# The main goal is to have a template to make the second version of flat in each of the four window in every CCD cameras of HiPERCAM.
# This makefile firstly creates a cube image containing the second flat.
# This flat image is created by combining with a mean the masked and normalized images in each CCD's window.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid images are going to be
bf2-outdir = $(BDIR)/flat2
bf2-masked-outdir =  $(BDIR)/masked
bf2-maskednorm-outdir =  $(BDIR)/maskednorm



# Define the directory where the images from HiPERCAM corrected by bias are taken
bf2-indir = $(BDIR)/i-filter





# Now define a sub-target. To create the final text file we have to use the masked images.
# This works as a loop: foreach input in the
# set of base-names writes in the outdirectory the name of the masked image. The same as the input one.
masked-images = $(foreach bf2-input,$(base-names),$(bf2-masked-outdir)/$(bf2-input).fits)





# Masking the images
# ------------------
#
# To create masked images we need the bias corrected images and the masks.
# Create a output directory
$(bf2-masked-outdir): ; mkdir $@
$(masked-images):$(bf2-masked-outdir)/%.fits: $(bf2-indir)/%.fits $(BDIR)/mask/%.fits | $(bf2-masked-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# masked data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the masked image
	# Masking each image by using astarithmetic.
	# Inject the masked data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 4);                                                      \
	do                                                                          \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                        \
	&& astarithmetic $< --hdu=$$n $(word 2,$^) --hdu=$$n 1 eq nan where --output=$$tmpim     \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	;done





# Pattern match for the masknormalized images. We use a loop to call all the images the same name of the 
# input one.
masknorm-images = $(foreach bf2-input,$(base-names),$(bf2-maskednorm-outdir)/$(bf2-input).fits)





# Normalize the masked images
# ---------------------------
#
# We use normalized images as variables to create the flat.
# To create the normalized images we need the masked ones taken from outdirectory.
# Normalization is done by dividing for the median value of the pixels each image.
$(bf2-maskednorm-outdir): ; mkdir $@
$(masknorm-images): $(bf2-maskednorm-outdir)/%.fits: $(bf2-masked-outdir)/%.fits | $(bf2-maskednorm-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# normalized data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image normalized
	# vnorm = normalization value obtained with the median
	# Normalize each image by dividing itself by vnorm
	# Inject the normalized data (tmpim, hdu=1) into the target
	# Remove the temporal image
	# you can use sigmaclip mean instead of median
	for n in $$(seq 1 4);                                          \
	do                                                              \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                            \
	&& vnorm=$$(aststatistics $< --hdu=$$n --median)                \
	&& astarithmetic $< --hdu=$$n $$vnorm / --output=$$tmpim        \
	&& astfits $$tmpim --copy=1 --output=$@                         \
	&& rm $$tmpim                                                   \
	; done





# Build the second version of the flat
# ------------------------------------
#
# To create the second flats we use the masked normalized images.
$(bf2-outdir): ; mkdir $@
$(bf2-outdir)/flats2.fits: $(masknorm-images) | $(bf2-outdir)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# flat data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the flat image
	# Building the flat by using astarithmetic.
	# Inject the flat data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 4);                                                     \
	do                                                                         \
	tmpim=$(bf2-outdir)/flat2-$$n.fits                                         \
	&& astarithmetic $^ -g$$n $(words $^) 3 0.2 sigclip-median --output=$$tmpim  \
	&& astfits $$tmpim --copy=1 --output=$@                                    \
	&& rm $$tmpim                                                              \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-flat2.tex: $(bf2-outdir)/flats2.fits | $(mtexdir)
	touch $@
