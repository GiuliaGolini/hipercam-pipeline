# Sky correction
#
# This Makefile is written to subctract the sky from the gain corrected images taken from HiPERCAM instrument.
# The main goal is to have a template to subctract the sky in each CCD cameras of HiPERCAM.
# To do that, a loop for reading all extensions is used.
# This routine is used because the sky area covered is very small and assuming there is no gradient for the sky value.
# Then the makefile create  cube images containing the final images.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Define the directory where the buid images are going to be
cs-outdir = $(BDIR)/corrected-sky-plane

cs-mask-outdir = $(BDIR)/cs-mask
cs-masked-outdir = $(BDIR)/cs-masked
cs-temp-outdir = $(BDIR)/cs-temp-fit

# Define the directory where the corrected for bias images from HiPERCAM are taken
cs-indir = $(BDIR)/corrected-gain-verified







# Now defining a sub-target. To create the final text file we have to use masked images.
# This works as a loop: foreach cs-input in the set of base-names writes in the outdirectory the name 
# by using the same input name.
cs-mask-images = $(foreach cs-input,$(base-names),$(cs-mask-outdir)/$(cs-input).fits)




# Build the mask of the CCD cameras images.
# -----------------------------------------
#
# We use mask-images as variables.
# To create the mask images we need the corrected for gain ones.
$(cs-mask-outdir): ; mkdir $@
$(cs-mask-images): $(cs-mask-outdir)/%.fits: $(cs-indir)/%.fits |$(cs-mask-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# mask data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the mask image
	# Create the mask of each image by using astnoisecheisel.
	# Note that the mask is the second output of astnoisecheisel and it is the only important image needed.
	# Inject the mask data (tmpim, hdu=2) into the target
	# Remove the temporal image
	# So now add hdu to the cube frame and at the same time read each hdu in the prerequisite image.
	for n in $$(seq 1 1);                                                                                \
	do                                                                                                   \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                                                 \
	&& astnoisechisel $< --hdu=$$n --output=$$tmpim --qthresh=0.15 --detgrowquant=0.6 --tilesize=25,25   \
	&& astfits $$tmpim --copy=2 --output=$@                                                              \
	&& rm $$tmpim                                                                                        \
	;done





# Now define a sub-target. To create the final text file we have to use the masked images.
# This works as a loop: foreach cs-input in the
# set of base-names writes in the outdirectory the name of the masked image by using the same input name.
cs-masked-images = $(foreach cs-input,$(base-names),$(cs-masked-outdir)/$(cs-input).fits)




# Mask the images
# ---------------
#
# To create masked images we need the corrected images and the masks.
$(cs-masked-outdir): ; mkdir $@
$(cs-masked-images):$(cs-masked-outdir)/%.fits: $(cs-indir)/%.fits $(cs-mask-outdir)/%.fits | $(cs-masked-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# masked data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the masked image
	# Masking each image by using astarithmetic.
	# Inject the masked data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 1);                                                       \
	do                                                                          \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                        \
	&& astarithmetic $^ --hdu=$$n --hdu=$$n  1 eq nan where --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	;done





# Now define another sub-target. To create the final text file we need to fit a 2d polinomial,
# related to the sky gradient clearly visuble in each extension image.
# This works as a loop: foreach cs-input in the
# set of base-names writes in the outdirectory the name of the fit by using the same input name.
cs-temp-images = $(foreach cs-input,$(base-names),$(cs-temp-outdir)/$(cs-input).fits)




# Sky estimation by fitting a 2d model
# ------------------------------------
# 
# To create the fit images we need the masked ones. Here we also define the rule to build the 
# temporary directory.
$(cs-temp-outdir): ; mkdir $@
$(cs-temp-images): $(cs-temp-outdir)/%.fits: $(cs-masked-outdir)/%.fits |$(cs-temp-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# polyfit data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the sky fit image
	# Call the python routine to calculate the fit.
	# Inject the data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 1);                                                       \
	do                                                                          \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                        \
	&& python reproduce/analysis/python/sky-fit.py $< $$n $$tmpim 1 None None   \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	;done




# This works as a loop: foreach cs-input image in the set of cs-inputs writes in the outdirectory the name 
# of the sky image substituting to the current
# name of the  data the extension _csky.fits
csky-images = $(foreach cs-input,$(base-names),$(cs-outdir)/$(cs-input).fits)




# Correct the image for the sky value
# -----------------------------------
#
#We use csky-images as variables.
#To create the corrected sky images we need the gain corrected ones and the masked ones.
$(cs-outdir): ; mkdir $@
$(csky-images): $(cs-outdir)/%.fits: $(cs-indir)/%.fits $(cs-temp-outdir)/%.fits | $(cs-outdir)
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# sky data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the sky image 
	# vsky = median value of the pixels of the masked image.
	# Correct the image by subtracting the median value to each image
	# Inject the sky data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 1);                                                      \
	do                                                                         \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                       \
	&& astarithmetic $< --hdu=$$n $(word 2,$^) --hdu=$$n - --output=$$tmpim    \
	&& astfits $$tmpim --copy=1 --output=$@                                    \
	&& rm $$tmpim                                                              \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-sky-plane.tex: $(csky-images)
	touch $@
