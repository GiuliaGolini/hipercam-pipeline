# Build the flat
#
# This Makefile is written to build the flat for the raw images taken from HiPERCAM instrument.
# The main goal is to have a template to make the flat in each of the four window in every CCD cameras of HiPERCAM.
# This makefile firstly does the normalization of all extensions present in a HiPERCAM raw image.
# The normalization is done taking the median value of
# pixels in every hdu image and by dividing each hdu image for its median value. 
# To do that, a loop for reading all extensions is used.
# Then the makefile create a cube image containing the flat.
# The flat is created combining with a sigma clip mean the dithering images in each CCD's window.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Define the directory where the images are going to be
bf-outdir = $(BDIR)/flat





# Define the directory where the images from HiPERCAM corrected by bias are taken
bf-indir = $(BDIR)/i-filter





# Define a temporary directory where the images normalized are
bf-tmpdir = $(BDIR)/bf-norm






# Now define a sub-target. To create the flat we need to use the normalized images.
# Define the name of the normalized images, the same name of the input image but in a different directory,
# It works as a loop foreach name in the set of base-names.
bf-normalized-images = $(foreach base-name,$(base-names),$(bf-tmpdir)/$(base-name).fits)





# Normalization of the image 
# --------------------------
#
# Use normalized images as variables to create the flat.
# To create the normalized images we need the raw ones taken from indirectory.
# Create also the output directory for normalized images.
$(bf-tmpdir): ; mkdir $@	
$(bf-normalized-images): $(bf-tmpdir)/%.fits: $(bf-indir)/%.fits | $(bf-tmpdir)
	# The 3 following steps are needed to create images with the same raw images format:
	# containing 20 sub-images corresponding to 4 window  
	# each filter (us gs  rs is zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# normalized data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image normalized
	# vnorm = normalization value obtained with the median
	# Normalize each image by dividing itself by vnorm
	# Inject the normalized data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 4);                                         \
	do                                                             \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                           \
	&& vnorm=$$(aststatistics $< --hdu=$$n --median)               \
	&& astarithmetic $< --hdu=$$n $$vnorm / --output=$$tmpim       \
	&& astfits $$tmpim --copy=1 --output=$@                        \
	&& rm $$tmpim                                                  \
	; done




# Building the first flat image 
# -----------------------------
#
# Define the rule needed to create the flat.
# To create the flats we use the normalized images.
# Create also the output directory.
$(bf-outdir): ; mkdir $@
$(bf-outdir)/flats.fits: $(bf-normalized-images) | $(bf-outdir)
	# The 3 following steps are needed to create images with the same raw images format:
	# 20 extensions (4 window each filter (us, gs, rs, is, zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	
	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# Steps in the loop:
	# tmpim = temporal name for the flat image
	# Create the flat image by doing a sigma clip mean using all the normalized images extension
	# Inject the flat data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 4);                                                           \
	do                                                                               \
	tmpim=$(bf-outdir)/flat-$$n.fits                                                 \
	&& astarithmetic $^ -g$$n $(words $^) 3 0.2 sigclip-median --output=$$tmpim      \
	&& astfits $$tmpim --copy=1 --output=$@                                          \
	&& rm $$tmpim                                                                    \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-flat.tex: $(bf-outdir)/flats.fits | $(mtexdir)
	touch $@