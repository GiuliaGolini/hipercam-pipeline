# Correction for masterbias
#
# This Makefile is written to correct all extensions present in a HiPERCAM raw image for the bias error.
# The main goal is to have a template to do the bias correction in each of the 
# four windows in every CCD cameras of HiPERCAM.
# The correction is done by subctracting from each extension in the raw image the corresponding extension bias.
# To do that, a loop for reading all 20 (4*5) extensions is used.
# The makefile at the end creates a cube image containing corrected windows images for each block
# of observations.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.




# Directories
# -----------
# Output directory where the images are going to be
cb-outdir = $(BDIR)/corrected-bias





# Define the directory where the bias images from HiPERCAM are taken
cb-indir = $(INDIR)/objects





# Define that all the raw-images are taken from the shell in the directory cb-indir,
# all the files ending with .fits
cb-inputs = $(wildcard $(cb-indir)/*.fits)
# Here we define the names of the images without the pwd and the extension. This is necessary to
# make the data reduction automatic. All makefiles will read these names for the images.
base-names = $(foreach cb-input,$(cb-inputs),$(notdir $(subst .fits,,$(cb-input))))
#$(info $(cb-base-names))
#exit 1




# Now define a sub-target. To create the final text file we have to use the images corrected for bias.
# This works as a loop: foreach name in the
# set of base-names writes in the outdirectory the name of the corrected for bias image. Same name of the input one
# but in another directory.
cbias-images = $(foreach base-name,$(base-names),$(cb-outdir)/$(base-name).fits)
#$(info $(cbias-images))
#exit 1





# Correct for bias image 
# ----------------------
#
# We use cbias-images as variables.
# To create the corrected images we need the ones taken from indirectory and the bias one previously built.
# Create also the output directory
$(cb-outdir): ; mkdir $@
$(cbias-images): $(cb-outdir)/%.fits: $(cb-indir)/%.fits $(BDIR)/bias/bias.fits | $(cb-outdir)
	# The 3 following steps are needed to create images with the same raw images
	# format: containing 20 sub-images corresponding to 4 window each filter (us gs  rs is zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0

	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# corrected data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the image corrected
	# Correction of each image by subtracting the masterbias image.
	# It is important to associate the correct hdu for each image.
	# Inject the corrected data (tmpim, hdu=1) into the target
	# Remove the temporal image
	for n in $$(seq 1 20);                                                            \
	do                                                                                \
	tmpim=$(subst .fits,-ext$$n.fits,$@)                                              \
	&& astarithmetic $< --hdu=$$n $(word 2,$^) --hdu=$$n - --output=$$tmpim           \
	&& astfits $$tmpim --copy=1 --output=$@                                           \
	&& rm $$tmpim                                                                     \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-bias.tex: $(cbias-images) | $(mtexdir)
	touch $@
