# Build the bias
#
# This Makefile is written to build the masterbias for the raw images taken from HiPERCAM instrument.
# The main goal is to have a template to make the masterbias in each of the four window in every CCD cameras of HiPERCAM.
# This makefile uses sigma clip mean between bias images of HiPERCAM.
# To do that, a loop for reading all 20 (4*5) extensions is used.
# The makefile at the end creates a cube image containing the masterbias image for each extension.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s): Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.



# Directories
# -----------
# Output directory where the buid images are going to be
bb-outdir = $(BDIR)/bias





# Define the directory where the bias images from HiPERCAM are taken
bb-indir = $(INDIR)/bias





# Define that the names of all the bias images are taken from the shell in the directory bb-indir,
# ending with .fits. An alternative form could be : bb-names = $(shell cd $(bb-indir) && ls *.fits)
bias-images := $(wildcard $(bb-indir)/*.fits) 





# Build bias image 
# ----------------------
#
# The image bias.fits is the image with all extensions masterbias.
# This target depends on the input bias-images.
# Here we also create the output directory
$(bb-outdir): ; mkdir $@
$(bb-outdir)/bias.fits: $(bias-images) | $(bb-outdir)
	# The 3 following steps are needed to create images with the same raw images format:
	# 20 extensions (4 window each filter (us, gs, rs, is, zs).
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@

	# By default, astfits has generated the extension 0 empty.
	# It has added the wanted hdu as the first extension of the
	# new image. We would like to keep the first extension with
	# the original information. To do that, we copy all keywords
	# from the original first extension into the first extension
	# of the new image.
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	
	# Now we have the target with two extensions. The first is the
	# wanted one because now it has header information. The second
	# is removed with the next command
	astfits $@ --remove=1

	# At this point we have an image with the header information in
	# the first extension. Now we fill all the extensions with the
	# data (20 = 5 filters x 4 windows).
	# Steps in the loop:
	# tmpim = temporal name for the bias image
	# Create the bias image by doing a sigma clip mean among all the bias images
	# Inject the bias data (tmpim, hdu=1) into the target
	# Remove the temporal image 
	for n in $$(seq 1 20);                                                      \
	do                                                                          \
	tmpim=$(bb-outdir)/bias-$$n.fits                                            \
	&& astarithmetic $^ -g$$n $(words $^) median --output=$$tmpim   \
	&& astfits $$tmpim --copy=1 --output=$@                                     \
	&& rm $$tmpim                                                               \
	; done





# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/build-bias.tex: $(bb-outdir)/bias.fits | $(mtexdir)
	touch $@