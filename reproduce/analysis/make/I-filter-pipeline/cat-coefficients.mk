# Working with coefficients
#
# This Makefile is written to correct all extensions present in a HiPERCAM image for the gain error:
# The coefficient that we want to join are the values corresponding at the sigma clip mean value of pixeles in
# the left/right bottom correction (coeff1 x 5 filters), the right/left upper correction(coeff2 x 5 filters),  
# the upper/lower correction(coeff3 x 5 filters).
# The final file all-coefficients.txt has 15 columns. 
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid file is going to be
cat-outdir = $(BDIR)/corrected-gain





# Define the directory where the files are
cat-indir = $(BDIR)/corrected-gain







# Now defining a sub-target. To create the final text file we have to use coefficient text files.
# This works as a loop: foreach input in the set of base-names takes in the outdirectory the name of the 
#coefficients file.
cgain-coefficients = $(foreach cat-input,$(base-names),$(cat-outdir)/$(cat-input).fits.txt)
#$(info $(cgain-coefficients))





# All coefficients table
#-----------------------
#
# Create the table with all the coefficients for all the images
#cgain-coefficients = $(wildcard $(cg-outdir)/*.txt) 
# To create this file we need the txt file with the coefficients for each image. Then add them with cat.
$(cat-outdir)/all-coefficients.txt: $(cgain-coefficients)
	cat $^ > $@




# Sigma clipping
#---------------
#
# Save the sigma clip mean value of each coefficient.
# To do this we need the table with all the coefficients and 
# We use a loop to read all the values.
# Steps in the loop:
# temp = temporal name for the sigma-clip-mean value of the column
# inject the temporal value into the target.
$(cat-outdir)/sigclip-means.txt: $(cat-outdir)/all-coefficients.txt
	for i in $$(seq 1 3);                                     \
	do                                                         \
	temp=$$(aststatistics $^ -c$$i --sigclip-mean)             \
	&& echo $$temp >> $@                                       \
	;done







# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/cat-coefficients.tex: $(cat-outdir)/all-coefficients.txt $(cat-outdir)/sigclip-means.txt 
	touch $@


