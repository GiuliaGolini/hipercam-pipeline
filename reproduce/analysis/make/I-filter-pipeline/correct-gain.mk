# Gain correction
#
# This Makefile is written to correct all extensions present in a HiPERCAM image for the gain error:
# HiPERCAM gain is 1.2 elect/adu.
# The main goal is to have a template to correct the gain for each filter images. 
# To do that, a loop for reading all extensions is used.
# The script calls a Python routine which is written to build the filter images (by combining the corresponding four window)
# and correct the gain error.
# Then the makefile creates cube images containing the CCD's images corrected for each block of observations.
#
# Original author:
# Giulia Golini <giulia.golini@gmail.com>
# Contributing author(s):Raul Infante-Sainz <infantesainz@gmail.com>
# Copyright (C) 2019, Giulia Golini.
#
# This Makefile is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
# Public License for more details. See <http://www.gnu.org/licenses/>.





# Directories
# -----------
# Define the directory where the buid images are going to be
cg-outdir = $(BDIR)/corrected-gain





# Define the directory where the corrected for bias images from HiPERCAM are taken
cg-indir = $(BDIR)/corrected-flat2






# Now defining a sub-target. To create the final text file we have to use the images corrected.
# This works as a loop: foreach input in the set of basenames writes in the outdirectory the name of the final image 
# ,the same name as the input image but in another directory.
cgain-images = $(foreach cg-input,$(base-names),$(cg-outdir)/$(cg-input).fits)




# Correct the image of gain
# -------------------------
#
# Use cgain-images as variables.
# To create the corrected images we need the ones taken from indirectory.
# Build the outdirectory.
# Call the python routine to calculate the median coefficients to correct of gain 
# Using 5 columns.
$(cg-outdir): ; mkdir $@
$(cgain-images): $(cg-outdir)/%.fits: $(cg-indir)/%.fits |$(cg-outdir)
	# Call the python routine 
	python3 reproduce/analysis/python/joining-verify.py $< $@ 3






# TeX macros final target 
# -----------------------
#
# This is how we write the necessary parameters in the final PDF.
#
# NOTE: In LaTeX you cannot use any non-alphabetic character in a variable
# name.
$(mtexdir)/correct-gain.tex: $(cgain-images) 
	touch $@


