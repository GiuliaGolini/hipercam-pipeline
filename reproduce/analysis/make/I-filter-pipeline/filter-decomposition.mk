# Script to divide the images in different filters
#
# We have to build 5 different directories with the 5 different bands
# the images in the different directories should corresponde to the first 
# 4 in the U and...






# Define each filter directory where the build images are going to be

dec-i = $(BDIR)/i-filter


ca-indir = $(BDIR)/corrected-bias







z-filter-images = $(foreach name, $(names), $(dec-z)/$(name))

$(dec-z):; mkdir $@
$(z-filter-images): $(dec-z)/%.fits: $(ca-indir)/%.fits | $(dec-z)
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1
	for n in $$(seq 17 20); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 




u-filter-images = $(foreach name, $(names), $(dec-u)/$(name))
$(dec-u):; mkdir $@
$(u-filter-images): $(dec-u)/%.fits: $(ca-indir)/%.fits | $(dec-u)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 1 4); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 





g-filter-images = $(foreach name, $(names), $(dec-g)/$(name))
$(dec-g):; mkdir $@
$(g-filter-images): $(dec-g)/%.fits: $(ca-indir)/%.fits | $(dec-g)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 5 8); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 





r-filter-images = $(foreach name, $(names), $(dec-r)/$(name))
$(dec-r):; mkdir $@
$(r-filter-images): $(dec-r)/%.fits: $(ca-indir)/%.fits | $(dec-r)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 9 12); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 





i-filter-images = $(foreach name, $(base-names), $(dec-i)/$(name).fits)
$(dec-i):; mkdir $@
$(i-filter-images): $(dec-i)/%.fits: $(ca-indir)/%.fits | $(dec-i)
	# The 3 following steps are needed to create images with the same raw images format.
	# Copy the first hdu into a new empty image
	astfits $<  --copy=0 --output=$@
	astfits $< --hdu=0 --copykeys=1:-1 --output=$@ --outhdu=0
	astfits $@ --remove=1

	for n in $$(seq 13 16); \
	do \
	astfits $< --copy=$$n --output=$@    \
	;done 



$(mtexdir)/filter-decomposition.tex:  $(i-filter-images) | $(mtexdir)
	touch $@




